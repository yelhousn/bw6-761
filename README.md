# bw6-761

A minimal implementation of the curve BW6-761 in Sage and Magma, from the paper
[ePrint 2020/351](https://eprint.iacr.org/2020/351), Youssef El Housni and
Aurore Guillevic.

See these GitHub links for other implementations:
- C++ in [libff](https://github.com/EYBlockchain/zk-swap-libff/tree/ey/libff/algebra/curves/bw6_761).
- Rust in [arkworks](https://github.com/arkworks-rs/curves/tree/master/bw6_761).
- Go in [gnark-crypto](https://github.com/ConsenSys/gnark-crypto/tree/master/ecc/bw6-761)

