from sage.all import Integer
"""
Pairing : Miller loop and final exponentiation
Formulas for add line and double line
Formulas for generic final exponentiation
and specific formulas for SW6 and BW6_761
preprint: https://eprint.iacr.org/2020/351
Optimized and secure pairing-friendly elliptic curves suitable for one layer proof composition
Youssef El Housni and Aurore Guillevic
"""

def double_line_J(S,P,a):
    """ computes 2*S and l_{S,S}(P) in Jacobian coordinates (X,Y,Z)
    where affine coordinates are (x,y) = (X/Z^2,Y/Z^3)

    if S has coordinates in F_{p^k/d} and P has coordinates in F_p, it costs:
    a=0:  5*s_{k/d} + 5*m_{k/d} + 2*k/d*m
    a=-3: 4*s_{k/d} + 6*m_{k/d} + 2*k/d*m
    a:    6*s_{k/d} + 5*m_{k/d} + 2*k/d*m
    if S has coordinates in F_p and P has coordinates in F_{p^k/d}, it costs:
    a=0:  5*s + 6*m + 2*k/d*m
    a=-3: 4*s + 7*m + 2*k/d*m
    a:    6*s + 6*m + 2*k/d*m
    """
    X,Y,Z,Z2 = S
    xP,yP = P
    t1 = Y**2                         # S
    t2 = 4*X*t1                       # M
    if a == 0:
        t3 = 3*X**2                   #   S  (a=0)
    elif a == -3:
        t3 = 3*(X-Z2)*(X+Z2)          #   M  (a=-3)
    else:
        t3 = 3*X**2 + a*Z2**2         #   2S (a any)
    X1= t3**2 - 2*t2                  # S
    Y1 = t3*(t2-X1)-8*t1**2           # M+S
    Z1 = Z*2*Y                        # M
    ld = Z1 * Z2                      # M
    ln = ld * yP - 2*t1 - t3*(Z2*xP-X)# M + 2*k/d*m_1
    # if S is in Fp and P in F_{p^{k/d}}, more efficient to compute:
    #ln = ld*yP-2*t1-(t3*Z2)*xP+t3*X  # 2M + 2*k/d*m_1
    return (ln, (X1,Y1,Z1,Z1**2))     # S

def add_line_J(S,Q,P):
    """ computes S+Q and l_{S,Q}(P) in Jacobian coordinates (X,Y,Z)
    where affine coordinates are (x,y) = (X/Z^2,Y/Z^3)
    If S,Q have coordinates in F_{p^k/d} and P has coordinates in F_p, it costs
    10*m_{k/d} + 3*s_{k/d}
    """
    X,Y,Z,Z2 = S
    xQ,yQ = Q
    xP,yP = P
    t1 = xQ*Z2 - X                    #  M
    t2 = yQ*Z*Z2-Y                    # 2M
    t3 = t1**2                        #  S
    t4 = t1*t3                        #  M
    t5 = X*t3                         #  M
    XX = t2**2 - (t4+2*t5)            #  S
    YY = t2*(t5-XX)-Y*t4              # 2M
    ZZ = Z*t1                         #  M
    ld = ZZ
    ln = ld *(yP-yQ) - t2*(xP-xQ)     # 2M
    return (ln, (XX,YY,ZZ,ZZ**2))     #  S

def double_line_J_CSB(S,P,a):
    """ computes 2*S and l_{S,S}(P) in Jacobian coordinates (X,Y,Z) and (x,y) = (X/Z^2,Y/Z^3)
        Chatterjee-Sarkar-Barua ICISC'04
    10*m_{k/d} + 3*s_{k/d}
    assume S has coordinates in F_{p^k/d} and P has coordinates in F_p
    """
    X1,Y1,Z1 = S
    x,y = P
                                # Tate, S in Fp           # ate, S in Fpk/2
    t1 = Y1**2                  # s                       # S
    t2 = 4*X1*t1                # m                       # M
    t3 = 8*t1**2                # s                       # S
    t4 = Z1**2                  # s                       # S
    if a == 0:
        t5 = 3*X1**2            # s                       # S
    elif a == -3:
        t5 = 3*(X1-t4)*(X1+t4)  # m                       # M
    else:
        t5 = 3*X1**2 + a*t4**2  # 2s                      # 2S
    X3 = t5**2 -2*t2            # s                       # S
    Y3 = t5*(t2-X3)-t3          # m                       # M
    Z3 = 2*Y1*Z1                # m                       # M
    l1 = Z3*t4*y                # m + k/2*m               # M + k/2*m
    #l0= -2*t1 - t5*(t4*x-X1)                             # k/2*m + M
    l0 = -2*t1 - t5*t4*x + t5*X1# 2m + k/2*m
    return (l0+l1,(X3,Y3,Z3))   # 6m + 5s + k*m if a=0    # 5M + 5S + k*m if a=0
                                # 7m + 4s + k*m if a=-3   # 6M + 4S + k*m if a=-3
                                # 6m + 6s + k*m if a any  # 5M + 6S + k*m if a any

def add_line_J_CSB(S,Q,P):
    """ computes S+Q and l_{S,Q}(P) in Jacobian coordinates (X,Y,Z) and (x,y) = (X/Z^2,Y/Z^3)
        Chatterjee-Sarkar-Barua ICISC'04
    10*m_{k/d} + 3*s_{k/d}
    """
    X1,Y1,Z1 = S
    xQ,yQ = Q
    xP,yP = P
    # if Tate: (x,y) = (xP,yP)
    # if ate:
    (x,y) = (xQ,yQ)
                                # Tate, S in Fp       # ate, S in F_{p^k/2}
    t1 = Z1**2                  # s                   # S
    t2 = Z1*t1                  # m                   # M
    t3 = x*t1                   # m                   # M
    t4 = y*t2                   # m                   # M
    t5 = t3-X1
    t6 = t4-Y1
    t7 = t5**2                  # s                   # S
    t8 = t5*t7                  # m                   # M
    t9 = X1*t7                  # m                   # M
    X3 = t6**2 - (t8+2*t9)      # s                   # S
    Y3 = t6*(t9-X3) - Y1*t8     # 2m                  # 2M
    Z3 = Z1*t5                  # m                   # M
    # Tate:
    #l1= Z3*yQ                  # k/2*m               #
    #l0= -Z3*yP - t6*(xQ - xP)  # m + k/2*m           #
    # ate:
    l1 = Z3*yP                  #                     # k/2*m
    l0 = -Z3*yQ - t6*(xP - xQ)  #                     # 2M
    return (l0+l1,(X3,Y3,Z3))   # 9m + k*m + 3s       # 10M + 3S + k/2*m

def bits_2naf(x):
    """
    This functions returns the binary non-adjacent form of x. It is
    uniquely defined. The i-th item in the returned list is the
    multiplier attached to 2^i in the 2-NAF.

    sage: bits_2naf(3456780)
    [0, 0, -1, 0, 1, 0, 0, 0, -1, 0, 0, 0, 0, 0, -1, 0, 1, 0, 1, 0, -1, 0, 1]
    https://gitlab.inria.fr/smasson/cocks-pinch-variant/blob/master/enumerate_sparse_T.py
    paper eprint 2019/431, DOI 10.1007/s10623-020-00727-w
    """
    L = []
    xx = Integer(x)
    assert x >= 0
    while xx > 0 :
        rr = xx % 4
        if rr == 3 :
            rr = -1
        else:
            rr = rr%2
        L.append(rr)
        xx -= rr
        xx,rr = xx.quo_rem(2)
        assert rr == 0
    assert x == sum([r*2**i for i,r in enumerate(L)])
    return L

def MillerFunction_ate(Q,P,a,T,m0=1):
    """
    computes the Miller function f_{T,Q}(P)
    Q,P are r-torsion points in affine coordinates,
    T is a scalar, T=(t-1) for ate pairing for example
    a is the curve coefficient in y^2=x^3+a*x+b (short Weierstrass)
    m0 is an optional parameter, for multi-exponentiation optimization,
    this is not needed for simple ate pairing, this is for SW6 and BW6.
    """
    m = m0
    S = (Q[0],Q[1],1,1) # extended Jacobian coordinates
    QQ = (Q[0],Q[1])
    PP = (P[0],P[1])
    loop = Integer(T).digits(2)
    for i in range(len(loop)-2, -1, -1):
        bi = loop[i]
        ln, S = double_line_J(S,PP,a)
        m = m**2 * ln
        if bi == 1:
            ln, S = add_line_J(S,QQ,PP)
            m = m*m0*ln
    return m, S

def MillerFunction_Tate(P,Q,a,r):
    return MillerFunction_ate(Q,P,a,r)

def MillerFunction_ate_CSB(Q,P,a,T,m0=1):
    """
    computes the Miller function f_{T,Q}(P)
    Q,P are r-torsion points in affine coordinates,
    T is a scalar, T=(t-1) for ate pairing for example
    a is the curve coefficient in y^2=x^3+a*x+b (short Weierstrass)
    m0 is an optional parameter, for multi-exponentiation optimization,
    this is not needed for simple ate pairing, this is for SW6 and BW6.
    """
    m = m0
    S = (Q[0],Q[1],1)
    QQ = (Q[0],Q[1])
    PP = (P[0],P[1])
    loop = Integer(T).digits(2)
    for i in range(len(loop)-2, -1, -1):
        bi = loop[i]
        ln, S = double_line_J_CSB(S,PP,a)
        m = m**2 * ln
        if bi == 1:
            ln, S = add_line_J_CSB(S,QQ,PP)
            m = m*m0*ln
    return m, S

def MillerFunction_Tate_CSB(P,Q,a,r):
    return MillerFunction_ate_CSB(Q,P,a,r)

def MillerFunction_ate_2NAF(Q,P,a,T,m0=1):
    """
    computes the Miller function f_{T,Q}(P)
    Q,P are r-torsion points in affine coordinates,
    T is a scalar, T=(t-1) for ate pairing for example
    a is the curve coefficient in y^2=x^3+a*x+b (short Weierstrass)
    m0 is an optional parameter, for multi-exponentiation optimization,
    this is not needed for simple ate pairing, this is for SW6.
    The loop iterates over T in 2-NAF representation.
    """
    m = m0
    m0_inv = 1/m0 # this costs one inversion in Fq^k
    S = (Q[0],Q[1],1,1)
    QQ = (Q[0],Q[1])
    QQ_ = (Q[0],-Q[1])
    PP = (P[0],P[1])
    loop = bits_2naf(T)
    for i in range(len(loop)-2, -1, -1):
        bi = loop[i]
        ln, S = double_line_J(S,PP,a)
        m = m**2 * ln
        if bi == 1:
            ln, S = add_line_J(S,QQ,PP)
            m = m*m0*ln
        elif bi == -1:
            ln, S = add_line_J(S,QQ_,PP)
            m = m*m0_inv*ln
    return m, S

def MillerFunction_Tate_2NAF(P,Q,a,r):
    return MillerFunction_ate_2NAF(Q,P,a,r)

def MillerLoop_opt_ate_SW6_test(Q,P,a,u):
    """
    non-optimized optimal ate Miller loop for SW6
    returns f_{u^3-u^2-u,Q}(P)*Frobenius(f_{u+1,Q}(P))
    Q, P are r-torsion points in G2, G1, in affine coordinates,
    u is the seed (integer) of the curve coefficients (r)
    a is the curve coefficient s.t. E: y^2 = x^3+a*x+b
    """
    m_u1,Su1 = MillerFunction_ate(Q,P,a,u+1)
    v = u*(u**2-u-1)
    m_v,Sv = MillerFunction_ate(Q,P,a,v)
    #m_v,Sv = MillerFunction_ate_2NAF(Q,P,a,v)
    Z1 = 1/Su1[2]
    Z2 = Z1**2
    Su1_ = (Su1[0]*Z2, Su1[1]*Z1*Z2)
    Z1 = 1/Sv[2]
    Z2 = Z1**2
    Sv_ = (Sv[0]*Z2, Sv[1]*Z1*Z2)
    return m_u1.frobenius() * m_v

def MillerLoop_opt_ate_SW6(Q,P,a,u):
    """
    Optimized optimal ate Miller loop for SW6
    returns f_{u*(u^2-u-1),Q}(P)*Frobenius(f_{u+1,Q}(P))
    v = (u^2-u-1)
    as Frobenius(f_{u+1,Q}(P)) * f^v_{u,Q}(P)*f_{v,[u]Q}(P)
    with a multi-exponentiation-like technique
    Q, P are r-torsion points in G2, G1, in affine coordinates,
    u is the seed (integer) of the curve coefficients,
    a is the curve coefficient y^2 = x^3+a*x+b
    """
    m_u,uS = MillerFunction_ate(Q,P,a,u)
    Z1 = 1/uS[2]
    Z2 = Z1**2
    uQ = (uS[0]*Z2, uS[1]*Z1*Z2)
    S = (uQ[0], uQ[1], 1,1)
    l, Qu1 = add_line_J(uS,(Q[0],Q[1]),(P[0],P[1]))
    m_u1 = m_u * l
    v = u**2-u-1
    m2,S2 = MillerFunction_ate(uQ,P,a,v, m0=m_u)
    #return m2**p * m_u1
    return m2 * m_u1.frobenius()

def MillerLoop_opt_ate_2NAF_SW6(Q,P,a,u):
    """
    Optimized optimal ate Miller loop for SW6
    returns f_{u*(u^2-u-1),Q}(P)*Frobenius(f_{u+1,Q}(P))
    v = (u^2-u-1)
    as Frobenius(f_{u+1,Q}(P)) * f^v_{u,Q}(P)*f_{v,[u]Q}(P)
    with a multi-exponentiation-like technique
    Q, P are r-torsion points in G2, G1, in affine coordinates,
    u is the seed (integer) of the curve coefficients,
    a is the curve coefficient y^2 = x^3+a*x+b
    """
    m_u,uS = MillerFunction_ate_2NAF(Q,P,a,u)
    Z1 = 1/uS[2]
    Z2 = Z1**2
    uQ = (uS[0]*Z2, uS[1]*Z1*Z2)
    S = (uQ[0], uQ[1], 1,1)
    l, Qu1 = add_line_J(uS,(Q[0],Q[1]),(P[0],P[1]))
    m_u1 = m_u * l
    v = u**2-u-1
    m2,S2 = MillerFunction_ate_2NAF(uQ,P,a,v, m0=m_u)
    #return m2**p * m_u1
    return m2 * m_u1.frobenius()

def MillerLoop_BW6_761_naive(Q,P,u,verbose=False):
    """
    non-optimized optimal ate Miller loop for SW6-bis
    the function MillerLoop_BW6_761 below is optimized
    returns f_{u+1,Q}(P)*Frobenius(f_{u*(u^2-u-1),Q}(P))
    Q, P are r-torsion points in G2, G1, in affine coordinates,
    u is the seed (integer) of the curve coefficients
    """
    m_u1,Su1 = MillerFunction_ate(Q,P,0,u+1)
    if verbose:
	Z1 = 1/Su1[2]
	Z2 = Z1**2
	S1 = (Su1[0]*Z2, Su1[1]*Z1*Z2,1,1)
	print("f_u1 = {}\nQu1 = {}\nQu1 = {}".format(m_u1, Su1, S1))
    v = u*(u**2-u-1)
    m_v,Sv = MillerFunction_ate(Q,P,0,v)
    #return m_u1 * m_v**p;
    return m_u1 * m_v.frobenius();

def MillerLoop_BW6_761(Q,P,u,verbose=False):
    """
    Optimized optimal ate Miller loop for SW6-bis
    returns f_{u+1,Q}(P)*Frobenius(f_{u*(u^2-u-1),Q}(P))
    v = (u^2-u-1)
    as f_{u+1,Q}(P) * Frobenius(f^v_{u,Q}(P)*f_{v,[u]Q}(P))
    with a multi-exponentiation-like technique
    Q, P are r-torsion points in G2, G1, in affine coordinates,
    u is the seed (integer) of the curve coefficients
    """
    m_u,uS = MillerFunction_ate(Q,P,0,u)
    Z1 = 1/uS[2]
    Z2 = Z1**2
    uQ = (uS[0]*Z2, uS[1]*Z1*Z2)
    S = (uQ[0], uQ[1], 1,1)
    l, Qu1 = add_line_J(uS,(Q[0],Q[1]),(P[0],P[1]))
    m_u1 = m_u * l
    v = u**2-u-1
    m2,S2 = MillerFunction_ate(uQ,P,0,v, m0=m_u)
    #return m2**p * m_u1
    return m2.frobenius() * m_u1

def MillerLoop_2NAF_BW6_761(Q,P,u,verbose=False):
    """
    Optimized optimal ate Miller loop for SW6-bis
    returns f_{u+1,Q}(P)*Frobenius(f_{u*(u^2-u-1),Q}(P))
    v = (u^2-u-1)
    as f_{u+1,Q}(P) * Frobenius(f^v_{u,Q}(P)*f_{v,[u]Q}(P))
    with a multi-exponentiation-like technique,
    and 2-NAF representation of scalars
    Q, P are r-torsion points in G2, G1, in affine coordinates,
    u is the seed (integer) of the curve coefficients
    """
    m_u,uS = MillerFunction_ate_2NAF(Q,P,0,u)
    Z1 = 1/uS[2]
    Z2 = Z1**2
    uQ = (uS[0]*Z2, uS[1]*Z1*Z2)
    S = (uQ[0], uQ[1], 1,1)
    l, Qu1 = add_line_J(uS,(Q[0],Q[1]),(P[0],P[1]))
    m_u1 = m_u * l
    v = u**2-u-1
    m2,S2 = MillerFunction_ate_2NAF(uQ,P,0,v, m0=m_u)
    #return m2**p * m_u1
    return m2.frobenius() * m_u1

def psi_sextic_M_twist(Q, alpha):
    """
    Returns the 6-th D-twist of a point Q in affine coordinates
    """
    return [Q[0]/alpha**2, Q[1]/alpha**3]

def psi_sextic_D_twist(Q, alpha):
    """
    Returns the 6-th M-twist of a point Q in affine coordinates
    """
    return [Q[0]*alpha**2, Q[1]*alpha**3]

#### AKLGL paper ####
def double_line_H_a0_twist6_AKLGL(S,P,b_t,D_twist=False):
    """
    Computes 2*S and l_{S,S}(P) in Homogeneous coordinates (X,Y,Z) and (x,y) = (X/Z,Y/Z)

    b' is the curve parameter for E', the sextic twist of E.
    E': y^2 = x^3 + b' = x^3 + b/xi and E' is a D-twist of E
    xi is the non-residue s.t. Fp6 = Fq[w]/(w^6-xi)
    
    For BLS12 curves, xi in Fp2
    For SW6-bis curve, xi in Fp.
    https://eprint.iacr.org/2010/526 Section 4 [Aranha Karabina Longa Gebotys Lopez Eurocrypt'11]
    Fp2 = Fp[i]/(i^2-beta), beta = -1
    Fp6 = Fp2[v]/(v^3-xi), xi = (1+i)
    Fp12 = Fp6[w]/(w^2 - v)
    E:  y^2 = x^3 + 2
    xi = (1+i)
    E': y^2 = x^3 + 2/(1+i) = x^3 + (1-i) -> b' = 1-i and E' is a D-twist
    """
    X1,Y1,Z1 = S
    xP,yP = P
    # Homogeneous coordinates: Z*Y^2 = X^3 + b'*Z^3 <=> (Y/Z)^2 = (X/Z)^3 + b'
    # note that Y^2 - b'*Z^2 = X^3/Z
    # D-twist: y^2 = x^3 + b/xi (multiply by xi=w^6) => w^6*y^2 = w^6*x^3 + b
    #                                                  <=> (w^3*y)^2 = (w^2*x)^3+b
    # psi: (x',y') -> (x'*w^2,y'*w^3) <-> (x',y'*w,1/w^2)
    # so that Y^2*Z is in Fq (no w^i term)
    #X3 = X1*Y1/2 * (Y1**2-9*b'*Z1**2)
    #Y3 = ((Y1**2+9*b'*Z1**2)/2)**2 - 27*b'**2*Z1**4
    #Z3 = 2*Y1**3*Z1
    #ln = ((-2*Y1*Z1*yP)*v*w + (3*X1**2*xP)*v**2 + xi*(3*b'*Z1**2-Y1**2))
    # with v=w^2, xi = w^6, this is
    #ln = xi*(3*b'*Z1**2-Y1**2) + (-2*Y1*Z1*yP)*w^3 + (3*X1**2*xP)*w**4
    
    A = X1*Y1/2            # M
    B = Y1**2              # S
    C = Z1**2              # S
    D = 3*C                #     3*Z1^2
    E = b_t*D              #     3*b_t*Z1^2
    F = 3*E                #     9*b_t*Z1^2
    X3 = A*(B-F)           # M   A*(B-9*b_t*C) = X1*Y1/2*(Y1^2-9*b_t*Z1^2)
    G = (B+F)/2            #     (Y1^2 + 9*b_t*Z1^2)/2
    Y3 = G**2-3*E**2       # 2S  (Y1^2 + 9*b_t*Z1^2)^2/4 -3*(3*b_t*Z1^2)^2
    H = (Y1+Z1)**2 - (B+C) # S   2*Y1*Z1
    Z3 = B*H               # M   2*Y1^3*Z1
    I = E-B                #     3*b_t*Z1^2-Y1^2
    J = X1**2              # S
    l3 = I
    l0 = H*(-yP)       # k/d*m
    l1 = 3*J*xP        # k/d*m
    if D_twist:
        ln = [l0,l1,0,l3,0,0]
        # AKLGL: multiply line by w^3 i.e. rotate by 3 to the right the vector -> now we have l3*w^6 = l3*xi at position 0
        #ln = (l3*xi,0,0,l0,l1,0)
        # and (l3*xi,0,l1) + (0,l0,0) as (Fq3)^2
        #     (l3*xi,0,xP*ll1) + (0,-yP*ll0,0)
    else:
        ln = [l3,0,l1,l0,0,0]
    return ln,(X3,Y3,Z3)   # 3M + 6S + 2*k/d*m

def double_line_AKLGL_test(S,P,b_t,w,D_twist=False):
    X1,Y1,Z1 = S
    xP,yP = P
    X3 = (X1*Y1)/2 * (Y1**2-9*b_t*Z1**2)
    Y3 = ((Y1**2+9*b_t*Z1**2)/2)**2 - 27*b_t**2*Z1**4
    Z3 = 2*Y1**3*Z1
    if D_twist:
        l = (-2*Y1*Z1*yP) + (3*X1**2*xP)*w + (3*b_t*Z1**2-Y1**2)*w**3
        lxy = ((3*b_t*Z1**2-Y1**2)*w**3, (3*X1**2)*w, (-2*Y1*Z1))
    else:
        l = (-2*Y1*Z1*yP)*w**3 + (3*X1**2*xP)*w**2 + (3*b_t*Z1**2-Y1**2)
        lxy = ((3*b_t*Z1**2-Y1**2), (3*X1**2)*w**2, (-2*Y1*Z1)*w**3)
    return l, (X3,Y3,Z3), lxy

def add_line_H_a0_twist6_AKLGL(S,Q,P,D_twist=False):
    (X1,Y1,Z1) = S
    (X2,Y2) = Q
    (xP,yP) = P
    t1 = Z1*X2 ; t2 = Z1*Y2     # 2M
    t1 = X1-t1 ; t2 = Y1-t2
    # D = X1 - Z1*X2 -> t1
    # E = Y1 - Y2*Z1 -> t2
    t3 = t1**2                  # S
    # F = D^2 -> t1^2 -> t3
    X3 = t3*X1 ; t4 = t2**2     # M+S
    # I = X1*F -> X3
    # G = E^2 -> t2^2 -> t4
    t3 = t1*t3 ; t4 = t4*Z1     # 2M
    # H = D*F -> t1*t3 -> t3
    # J = H + Z1*G - 2*I -> t3 + t4 -2*X3
    t4 = t4+t3
    t4 = t4-X3
    t4 = t4-X3 # J -> t4
    X3 = X3-t4
    T1 = t2*X3 ; T2 = t3*Y1     # 2M
    T2 = T1 - T2
    Y3 = T2; X3=t1*t4; Z3=t3*Z1 # 2M
    lx = -t2*xP                 # k/d*m  lx=-(Y1-Z1*Y2)*xP
    T1 = t2*X2                  # M
    T2 = t1*Y2                  # M
    l0 = T1 - T2                #        l0=(Y1-Z1*Y2)*X2-(X1-Z1*X2)*Y2
    ly = t1*yP                  # k/d*m  ly=(X1-Z1*X2)*yP
    if D_twist:
        ln = [ly,lx,0,l0,0,0] # *w^3 -> (l0*xi,0,0,ly,lx,0)
    else:
        ln = [l0,0,lx,ly,0,0]
    return ln,(X3,Y3,Z3) # 11M + 2S + 2*k/d*m

def add_line_AKLGL_test(S,Q,P,w,D_twist=False):
    """
    Fp2 = Fp[i]/(i^2-beta), beta = -1
    Fp6 = Fp2[v]/(v^3-xi), xi = (1+i)
    Fp12 = Fp6[w]/(w^2 - v)
    """
    (X1,Y1,Z1) = S
    (X2,Y2) = Q
    (xP,yP) = P
    T = Y1-Y2*Z1
    L = X1 - X2*Z1
    X3 = L*(L**3+Z1*T**2-2*X1*L**2)
    Y3 = T*(3*X1*L**2 - L**3 - Z1*T**2) - Y1*L**3
    Z3 = Z1*L**3
    if D_twist:
        l = L*(yP) - (T*xP)*w + (T*X2-L*Y2)*w**3
        lxy = ((T*X2-L*Y2)*w**3, -T*w, L)
    else:
        l = (T*X2-L*Y2) - (T*xP)*w**2 + L*(yP)*w**3
        lxy = ((T*X2-L*Y2), -T*w**2, L*w**3)
    return l, (X3,Y3,Z3), lxy

def sparse_mult_M6_twist(l0,l2,l3, f, xi, Fq6):
    # source: PhD thesis A. Guillevic 2013 p. 91 Sect. 3.2.2
    # https://tel.archives-ouvertes.fr/tel-00921940
    #print("f= {}\nf.polynomial()={}\nf.polynomial().list()={}\n".format(f, f.polynomial(), f.polynomial().list()))
    coeffs = f.polynomial().list()
    if len(coeffs) < 6:
        coeffs += [0]*(6-len(coeffs))
    (f0,f1,f2,f3,f4,f5) = coeffs
    l0f0 = l0*f0
    l2f2 = l2*f2
    l3f5 = l3*f5
    h2 = xi*l3f5 + (l0+l2)*(f0+f2) - l0f0 - l2f2
    l2f4 = l2*f4
    l3f3 = l3*f3
    h0 = l0f0 + xi*(l2f4 + l3f3)
    l2f1 = l2*f1
    h3 = l2f1 + (l0+l3)*(f0+f3) - l0f0 - l3f3
    l0f4 = l0*f4
    l3f1 = l3*f1
    h4 = l2f2 + l0f4 + l3f1
    l0f5 = l0*f5
    h5 = l0f5 + (l2+l3)*(f2+f3) - l2f2 - l3f3
    h1 = (l0+l2+l3)*(f1 + xi*(f4+f5)) - xi*(l0f4 + l0f5 + l2f4 + l3f5) - l2f1 - l3f1
    return Fq6([h0,h1,h2,h3,h4,h5])

def sparse_mult_D6_twist(l0,l1,l3, f, xi, Fq6):
    # source: PhD thesis A. Guillevic 2013 p. 91 Sect. 3.2.2
    # https://tel.archives-ouvertes.fr/tel-00921940
    #print("f= {}\nf.polynomial()={}\nf.polynomial().list()={}\n".format(f, f.polynomial(), f.polynomial().list()))
    coeffs = f.polynomial().list()
    if len(coeffs) < 6:
        coeffs += [0]*(6-len(coeffs))
    (f0,f1,f2,f3,f4,f5) = coeffs
    l1f5 = l1*f5
    l0f0 = l0*f0
    l3f3 = l3*f3
    h0 = l0f0 + xi*(l1f5 + l3f3)
    l1f1 = l1*f1
    l3f4 = l3*f4
    h1 = (l0+l1)*(f0+f1) - l0f0 - l1f1 + xi*l3f4
    l0f2 = l0*f2
    l3f5 = l3*f5
    h2 = l0*f2 + l1*f1 + xi*l3*f5
    l1f2 = l1*f2
    h3 = l1f2 + (l0+l3)*(f0+f3) - l0f0 - l3f3
    l0f4 = l0*f4
    h4 = l0f4 + (l1+l3)*(f1+f3) - l1f1 - l3f3
    h5 = (l0+l1+l3)*(f2+f4+f5) - (l0f4 + l1f2 + l1f5 + l3f4 + l0f2 + l3f5)
    return Fq6([h0,h1,h2,h3,h4,h5])

def MillerFunction_ate_AKLGL(Q,P,b_t,T,Fq6,D_twist=False,m0=1):
    xi = -Fq6.polynomial().constant_coefficient()
    m = m0
    S = (Q[0],Q[1],1)
    QQ = (Q[0],Q[1])
    PP = (P[0],P[1])
    loop = Integer(T).digits(2)
    # very first step: no "m = m**2" needed
    bi = loop[len(loop)-2]
    ln, S = double_line_H_a0_twist6_AKLGL(S,PP,b_t,D_twist=D_twist)
    if m0 != 1:
        m = m**2
        if D_twist:
            l0 = ln[0] ; l1 = ln[1] ; l3 = ln[3]
            m = sparse_mult_D6_twist(l0,l1,l3, m, xi, Fq6)
        else:
            l0 = ln[0] ; l2 = ln[2] ; l3 = ln[3]
            m = sparse_mult_M6_twist(l0,l2,l3, m, xi, Fq6)
    else:
        m = Fq6(ln)
    if bi == 1:
        if m0 != 0:
            m = m*m0
        ln, S = add_line_H_a0_twist6_AKLGL(S,QQ,PP,D_twist=D_twist)
        if D_twist:
            l0 = ln[0] ; l1 = ln[1] ; l3 = ln[3]
            m = sparse_mult_D6_twist(l0,l1,l3, m, xi, Fq6)
        else:
            l0 = ln[0] ; l2 = ln[2] ; l3 = ln[3]
            m = sparse_mult_M6_twist(l0,l2,l3, m, xi, Fq6)
    
    for i in range(len(loop)-3, -1, -1):
        bi = loop[i]
        ln, S = double_line_H_a0_twist6_AKLGL(S,PP,b_t,D_twist=D_twist)
        m = m**2
        if D_twist:
            l0 = ln[0] ; l1 = ln[1] ; l3 = ln[3]
            m = sparse_mult_D6_twist(l0,l1,l3, m, xi, Fq6)
        else:
            l0 = ln[0] ; l2 = ln[2] ; l3 = ln[3]
            m = sparse_mult_M6_twist(l0,l2,l3, m, xi, Fq6)
        if bi == 1:
            if m0 != 0:
                m = m*m0
            ln, S = add_line_H_a0_twist6_AKLGL(S,QQ,PP,D_twist=D_twist)
            if D_twist:
                l0 = ln[0] ; l1 = ln[1] ; l3 = ln[3]
                m = sparse_mult_D6_twist(l0,l1,l3, m, xi, Fq6)
            else:
                l0 = ln[0] ; l2 = ln[2] ; l3 = ln[3]
                m = sparse_mult_M6_twist(l0,l2,l3, m, xi, Fq6)
    return m, S

def MillerFunction_ate_2NAF_AKLGL(Q,P,b_t,T,Fq6,D_twist=False,m0=1):
    """
    computes the Miller function f_{T,Q}(P) with AKLGL Dbl/Add formulas
    Q,P are r-torsion points in affine coordinates,
    T is a scalar, T=(t-1) for ate pairing for example
    b_t is the 6-twist curve coefficient in y^2=x^3+b_t (short Weierstrass)
    m0 is an optional parameter, for multi-exponentiation optimization,
    this is not needed for simple ate pairing, this is for SW6 and BW6_761.
    The loop iterates over T in 2-NAF representation.
    """
    xi = -Fq6.polynomial().constant_coefficient()
    m = m0
    if m0 != 1:
        m0_inv = 1/m0 # this costs one inversion in Fq^k
    else:
        m0_inv = 1
    S = (Q[0],Q[1],1)
    QQ = (Q[0],Q[1])
    QQ_ = (Q[0],-Q[1])
    PP = (P[0],P[1])
    loop = bits_2naf(T)
    # very first step: no "m = m**2" needed
    bi = loop[len(loop)-2]
    ln, S = double_line_H_a0_twist6_AKLGL(S,PP,b_t,D_twist=D_twist)
    if m0 != 1:
        m = m**2
        if D_twist:
            l0 = ln[0] ; l1 = ln[1] ; l3 = ln[3]
            m = sparse_mult_D6_twist(l0,l1,l3, m, xi, Fq6)
        else:
            l0 = ln[0] ; l2 = ln[2] ; l3 = ln[3]
            m = sparse_mult_M6_twist(l0,l2,l3, m, xi, Fq6)
    else:
        m = Fq6(ln)
    if bi == 1:
        if m0 != 1:
            m = m*m0
        ln, S = add_line_H_a0_twist6_AKLGL(S,QQ,PP,D_twist=D_twist)
        if D_twist:
            l0 = ln[0] ; l1 = ln[1] ; l3 = ln[3]
            m = sparse_mult_D6_twist(l0,l1,l3, m, xi, Fq6)
        else:
            l0 = ln[0] ; l2 = ln[2] ; l3 = ln[3]
            m = sparse_mult_M6_twist(l0,l2,l3, m, xi, Fq6)
    elif bi == -1:
        if m0 != 1:
            m = m*m0_inv
        ln, S = add_line_H_a0_twist6_AKLGL(S,QQ_,PP,D_twist=D_twist)
        if D_twist:
            l0 = ln[0] ; l1 = ln[1] ; l3 = ln[3]
            m = sparse_mult_D6_twist(l0,l1,l3, m, xi, Fq6)
        else:
            l0 = ln[0] ; l2 = ln[2] ; l3 = ln[3]
            m = sparse_mult_M6_twist(l0,l2,l3, m, xi, Fq6)
    
    for i in range(len(loop)-3, -1, -1):
        bi = loop[i]
        ln, S = double_line_H_a0_twist6_AKLGL(S,PP,b_t,D_twist=D_twist)
        m = m**2
        if D_twist:
            l0 = ln[0] ; l1 = ln[1] ; l3 = ln[3]
            m = sparse_mult_D6_twist(l0,l1,l3, m, xi, Fq6)
        else:
            l0 = ln[0] ; l2 = ln[2] ; l3 = ln[3]
            m = sparse_mult_M6_twist(l0,l2,l3, m, xi, Fq6)
        if bi == 1:
            if m0 != 1:
                m = m*m0
            ln, S = add_line_H_a0_twist6_AKLGL(S,QQ,PP,D_twist=D_twist)
            if D_twist:
                l0 = ln[0] ; l1 = ln[1] ; l3 = ln[3]
                m = sparse_mult_D6_twist(l0,l1,l3, m, xi, Fq6)
            else:
                l0 = ln[0] ; l2 = ln[2] ; l3 = ln[3]
                m = sparse_mult_M6_twist(l0,l2,l3, m, xi, Fq6)
        elif bi == -1:
            if m0 != 1:
                m = m*m0_inv
            ln, S = add_line_H_a0_twist6_AKLGL(S,QQ_,PP,D_twist=D_twist)
            if D_twist:
                l0 = ln[0] ; l1 = ln[1] ; l3 = ln[3]
                m = sparse_mult_D6_twist(l0,l1,l3, m, xi, Fq6)
            else:
                l0 = ln[0] ; l2 = ln[2] ; l3 = ln[3]
                m = sparse_mult_M6_twist(l0,l2,l3, m, xi, Fq6)
    return m, S

def MillerLoop_ate_AKLGL_naive(Q,P,b_t,u,Fq6,D_twist=False):
    Q_ = (Q[0],Q[1],1)
    P_ = (P[0],P[1])
    u1 = u+1
    mu, Su = MillerFunction_ate_AKLGL(Q_,P_,b_t,u1,Fq6,m0=1,D_twist=D_twist)
    #Qu1 = (u+1)*Q
    #print("S == (u+1)*Q: {}".format(Su[0]/Su[2] == Qu1[0] and Su[1]/Su[2] == Qu1[1]))
    v = u*(u**2-u-1)
    mv, Sv = MillerFunction_ate_AKLGL(Q_,P_,b_t,v,Fq6,m0=1,D_twist=D_twist)
    #Qv = v*Q
    #print("S == v*Q: {}".format(Sv[0]/Sv[2] == Qv[0] and Sv[1]/Sv[2] == Qv[1]))
    f = mu*mv.frobenius()
    return f

def MillerLoop_ate_AKLGL(Q,P,b_t,u,Fq6,D_twist=False):
    xi = -Fq6.polynomial().constant_coefficient()
    Q_ = (Q[0],Q[1],1)
    P_ = (P[0],P[1])
    u1 = u+1
    mu, uS = MillerFunction_ate_AKLGL(Q_,P_,b_t,u,Fq6,m0=1,D_twist=D_twist)
    Z1 = 1/uS[2]
    uQ = (uS[0]*Z1, uS[1]*Z1) # homogeneous projective coordinates
    S = (uQ[0], uQ[1], 1)
    l, Qu1 = add_line_H_a0_twist6_AKLGL(uS,(Q[0],Q[1]),P_,D_twist=D_twist)
    if D_twist:
        l0 = l[0] ; l1 = l[1] ; l3 = l[3]
        m_u1 = sparse_mult_D6_twist(l0,l1,l3, mu, xi, Fq6)
    else:
        l0 = l[0] ; l2 = l[2] ; l3 = l[3]
        m_u1 = sparse_mult_M6_twist(l0,l2,l3, mu, xi, Fq6)
    v = u**2-u-1
    mv, Sv = MillerFunction_ate_AKLGL(uQ,P_,b_t,v,Fq6,m0=mu,D_twist=D_twist)
    f = m_u1*mv.frobenius()
    return f

def MillerLoop_ate_2NAF_AKLGL(Q,P,b_t,u,Fq6,D_twist=False):
    xi = -Fq6.polynomial().constant_coefficient()
    Q_ = (Q[0],Q[1],1)
    P_ = (P[0],P[1])
    u1 = u+1
    mu, uS = MillerFunction_ate_2NAF_AKLGL(Q_,P_,b_t,u,Fq6,m0=1,D_twist=D_twist)
    Z1 = 1/uS[2]
    uQ = (uS[0]*Z1, uS[1]*Z1) # homogeneous projective coordinates
    S = (uQ[0], uQ[1], 1)
    l, Qu1 = add_line_H_a0_twist6_AKLGL(uS,(Q[0],Q[1]),P_,D_twist=D_twist)
    if D_twist:
        l0 = l[0] ; l1 = l[1] ; l3 = l[3]
        m_u1 = sparse_mult_D6_twist(l0,l1,l3, mu, xi, Fq6)
    else:
        l0 = l[0] ; l2 = l[2] ; l3 = l[3]
        m_u1 = sparse_mult_M6_twist(l0,l2,l3, mu, xi, Fq6)
    v = u**2-u-1
    mv, Sv = MillerFunction_ate_2NAF_AKLGL(uQ,P_,b_t,v,Fq6,m0=mu,D_twist=D_twist)
    f = m_u1*mv.frobenius()
    return f

#### Exponentiations ####
def multi_exp(f0, e0, f1, e1):
    # returns f_0^e0 * f_1^e1
    # assumes e0 > 0, e1 > 1
    if e0 < e1:
	ee = e0
	e0 = e1
	e1 = ee
	ff = f0
	f0 = f1
	f1 = ff
    E0 = Integer(e0).digits(2)
    E1 = Integer(e1).digits(2)
    f = f0
    S = 0
    M = 0
    for i in range(len(E0)-2, len(E1)-2, -1): # for i= # E0-1 to # E1 by -1 do
	f = f**2;         S += 1
	bi = E0[i]
	if bi == 1:
	    f = f * f0;  M += 1
    f = f * f1;          M += 1
    f0f1 = f0*f1;        M += 1
    for i in range(len(E1)-2, -1, -1): # i= # E1-1 to 1 by -1 do
	f = f**2;         S += 1
	bi = E0[i]
	ci = E1[i]
	if bi == 1 and ci == 0:
	    f *= f0;     M += 1
	elif bi == 0 and ci == 1:
	    f *= f1;     M += 1
	elif bi == 1 and ci == 1:
	    f *= f0f1;   M += 1
    print("multi-exp: {}S + {}M".format(S,M))
    return f

def multi_exp_2NAF(f0,f0_inv,f1,f1_inv,E0_2NAF,E1_2NAF, verbose=False):
    # returns f_0^E0_2NAF * f_1^E1_2NAF
    # assumes e0 > 0, e1 > 1
    f = f0
    S = 0
    M = 0
    for i in range(len(E0_2NAF)-2, len(E1_2NAF)-2, -1): # i= # E0_2NAF-1 to # E1_2NAF by -1 do
	f = f**2;               S += 1
	bi = E0_2NAF[i]
	if bi == 1:
	    f *= f0;           M += 1
	elif bi == -1:
	    f *= f0_inv;       M += 1
    # precomputations
    f = f * f1;                M += 1
    f0f1 = f0*f1;              M += 1
    f0if1 = f0_inv*f1;         M += 1
    f0f1i = f0*f1_inv;         M += 1
    f0if1i = f0_inv*f1_inv;    M += 1
    for i in range(len(E1_2NAF)-2, -1, -1): # i= # E1_2NAF-1 to 1 by -1 do
	f = f**2;               S += 1
	bi = E0_2NAF[i]
	ci = E1_2NAF[i]
	if bi == 1 and ci == 0:
	    f *= f0;           M += 1
	elif bi == 0 and ci == 1:
	    f *= f1;           M += 1
	elif bi == 1 and ci == 1:
	    f *= f0f1;         M += 1
	elif bi == -1 and ci == 0:
	    f *= f0_inv;       M += 1
	elif bi == 0 and ci == -1:
	    f *= f1_inv;       M += 1
	elif bi == 1 and ci == -1:
	    f *= f0f1i;        M += 1
	elif bi == -1 and ci == 1:
	    f *= f0if1;        M += 1
	elif bi == -1 and ci == -1:
	    f *= f0if1i;       M += 1
    if verbose:
	print("multi-exp: {}S + {}M".format(S,M))
    return f, S,M

def FinalExp_easy_SW6(m):
    # assumes Fq6 = (Fq3)^2 that is, first a cubic extension, then above, a quadratic ext.
    mp3 = m.frobenius(3)
    im = 1/m
    f = mp3*im # m^(q^3-1)
    f = f * f.frobenius() # m^((q^3-1)*(q+1))
    return f

def FinalExp_hard_SW6(m):
    # returns m^(-W0 + q*W1) = (m^(-1))^W0 * (m^q)^W1
    W1_2NAF=[0,1,0,1,0,-1,0,0,-1,0,-1,0,-1,0,1,0,0,0,1,0,1,0,0,-1,0,0,-1,0,0,0,0,-1,0,-1,0,-1,0,0,-1,0,0,1,0,-1,0,0,-1,0,0,0,0,-1,0,-1,0,1,0,-1,0,-1,0,-1,0,-1,0,0,0,0,-1,0,-1,0,-1,0,0,0,1,0,1,0,1,0,1,0,1,0,-1,0,0,0,-1,0,0,0,1,0,-1,0,0,0,0,-1,0,1,0,-1,0,1,0,1,0,-1,0,1,0,-1,0,0,0,0,1,0,1,0,0,0,0,-1,0,0,0,1,0,1,0,0,1,0,0,0,1,0,-1,0,1,0,-1,0,0,-1,0,-1,0,0,1,0,0,-1,0,0,1,0,1,0,1,0,0,0,0,1,0,0,0,0,1,0,0,1,0,1,0,-1,0,0,0,-1,0,1,0,0,0,0,1,0,-1,0,0,1,0,0,0,0,0,0,1,0,1,0,1,0,0,0,0,0,-1,0,-1,0,0,0,1,0,1,0,1,0,0,0,0,0,-1,0,0,0,-1,0,1,0,-1,0,0,0,-1,0,1,0,-1,0,0,0,1,0,0,-1,0,0,0,0,-1,0,-1,0,-1,0,-1,0,1,0,1,0,0,1,0,0,0,0,-1,0,1,0,0,1,0,-1,0,-1,0,1,0,0,1,0,0,0,0,-1,0,0,-1,0,1,0,-1,0,-1,0,0,-1,0,1,0,-1,0,1,0,0,-1,0,0,0,0,0,0,-1,0,1,0,0,0,0,0,0,1,0,0,-1,0,1,0,0,0,-1,0,0,1,0,0,0,1,0,-1,0,-1,0,-1,0,0,0,0,1,0,1,0,0,0,-1,0,1,0,0,1,0,1,0,0,0,0,-1,0,0,0,0,0,1,0,0,1,0,-1,0,-1,0,0,-1,0,0,0,0,0,-1,0,1,0,0,0,1]
    W0_2NAF=[1,0,0,0,1,0,-1,0,0,-1,0,-1,0,-1,0,1,0,0,0,1,0,1,0,0,-1,0,0,-1,0,0,0,0,-1,0,-1,0,-1,0,0,-1,0,0,1,0,-1,0,0,0,-1,0,0,0,-1,0,1,0,0,-1,0,-1,0,0,-1,0,-1,0,-1,0,0,0,0,0,1,0,0,0,1,0,0,1,0,1,0,0,1,0,-1,0,0,1,0,1,0,0,1,0,-1,0,0,0,-1,0,0,0,1,0,-1,0,0,-1,0,-1,0,0,-1,0,-1,0,0,-1,0,0,0,1,0,-1,0,-1,0,0,0,1,0,1,0,0,0,0,1,0,-1,0,0,1,0,0,1,0,1,0,0,1,0,0,0,0,0,-1,0,1,0,0,-1,0,0,0,1,0,-1,0,0,0,1,0,-1,0,0,0,0,0,0,0,0,-1,0,1,0,0,1,0,-1,0,0,1,0,1,0,0,1,0,0,0,0,1,0,1,0,0,-1,0,-1,0,0,1,0,0,0,0,-1,0,0,0,-1,0,-1,0,1,0,1,0,0,-1,0,1,0,-1,0,-1,0,0,0,0,1,0,1,0,0,-1,0,0,-1,0,0,-1,0,-1,0,0,-1,0,1,0,-1,0,-1,0,1,0,-1,0,0,1,0,0,0,0,1,0,-1,0,1,0,-1,0,-1,0,0,0,1,0,-1,0,0,0,0,1,0,0,0,1,0,0,0,-1,0,1,0,0,-1,0,0,-1,0,1,0,0,0,1,0,0,0,1,0,1,0,0,1,0,-1,0,0,0,-1,0,0,-1,0,1,0,0,1,0,1,0,1,0,0,0,-1,0,0,0,0,0,1,0,-1,0,0,1,0,0,0,1,0,1,0,0,0,1,0,-1,0,1,0,-1,0,0,0,1,0,0,0,1,0,-1,0,0,1,0,1,0,0,1,0,0,-1,0,0,0,0,-1,0,0,1,0,-1,0,-1,0,1,0,1,0,-1,0,0,0,-1,0,-1,0,0,-1,0,-1,0,1,0,0,0,1,0,-1,0,1,0,0,-1,0,1,0,-1,0,1,0,0,-1,0,0,1,0,1,0,0,-1,0,1,0,-1,0,1,0,0,0,0,0,0,1,0,-1,0,1,0,1,0,0,0,1,0,0,-1,0,1,0,-1,0,-1,0,0,-1,0,0,0,0,0,1,0,1,0,-1,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,1,0,-1,0,1,0,-1,0,1,0,1,0,1,0,-1,0,0,0,1,0,0,-1,0,1,0,0,-1,0,0,0,0,1,0,0,1,0,1,0,-1,0,0,-1,0,0,0,0,0,-1,0,1,0,-1,0,0,-1,0,0,0,-1,0,-1,0,0,1,0,-1,0,0,0,0,1,0,1,0,0,1,0,0,-1,0,-1,0,1,0,0,0,0,0,1,0,-1,0,-1,0,0,0,1,0,0,1,0,-1,0,0,0,-1,0,0,-1,0,-1,0,1,0,1,0,0,-1,0,0,0,0,-1,0,1,0,-1,0,0,0,0,0,0,0,0,0,1,0,-1,0,-1,0,1,0,-1,0,1,0,-1,0,0,1,0,0,0,0,1,0,0,0,0,-1,0,-1,0,-1,0,1,0,0,1,0,-1,0,-1,0,-1,0,-1,0,-1,0,0,1,0,0,1,0,0,1,0,-1,0,-1,0,0,0,1,0,1,0,0,0,1,0,1]
    f0 = m
    f0_inv = f0.frobenius(3) # f0^(q^3) = 1/f0
    f1 = m.frobenius()
    f1_inv = f1.frobenius(3) # f1^(q^3) = 1/f1
    f,S,M = multi_exp_2NAF(f0_inv,f0,f1,f1_inv,W0_2NAF,W1_2NAF,verbose=False)
    return f

def FinalExp_SW6(m):
    f = FinalExp_easy_SW6(m)
    f = FinalExp_hard_SW6(f)
    return f

def atePairing_SW6(Q,P,a,tr):
    T = tr-1
    m,S1 = MillerFunction_ate(Q,P,a,T)
    f = FinalExp_SW6(m)
    return f

def atePairing_SW6_CSB(Q,P,a,tr):
    T = tr-1
    m,S1 = MillerFunction_ate_CSB(Q,P,a,T)
    f = FinalExp_SW6(m)
    return f

def atePairing_SW6_2NAF(Q,P,a,tr):
    T = tr-1
    m,S1 = MillerFunction_ate_2NAF(Q,P,a,T)
    f = FinalExp_SW6(m)
    return f

def TatePairing_SW6(P,Q,a,r):
    m,S1 = MillerFunction_Tate(P,Q,a,r)
    f = FinalExp_SW6(m)
    return f

def TatePairing_SW6_CSB(P,Q,a,r):
    m,S1 = MillerFunction_Tate_CSB(P,Q,a,r)
    f = FinalExp_SW6(m)
    return f

def TatePairing_SW6_2NAF(P,Q,a,r):
    m,S1 = MillerFunction_Tate_2NAF(P,Q,a,r)
    f = FinalExp_SW6(m)
    return f

def FinalExp_easy_BW6_761(m):
    mp3 = m.frobenius(3) # assuming the extension is in one layer
    im = 1/m
    f = mp3*im; # m^(q^3-1)
    f = f * f.frobenius() # m^((q^3-1)*(q+1))
    return f

def FinalExp_hard_BW6_761(m,u0):
    f1 = m**u0
    f2 = f1**u0
    f3 = f2**u0
    f4 = f3**u0
    f5 = f4**u0
    f6 = f5**u0
    f7 = f6**u0
    fR0 = (m**220*f1**263*f2**73*f3**314*f4**197*f7**103).frobenius(3) * f5**269*f6**70
    fp = m.frobenius()
    f1p = f1.frobenius()
    f2p = f2.frobenius()
    f3p = f3.frobenius()
    f4p = f4.frobenius()
    f5p = f5.frobenius()
    f6p = f6.frobenius()
    f7p = f7.frobenius()
    f8p = f7p**u0
    f9p = f8p**u0
    fR1 = fp**229*f1p**34*f3p**452*f6p**492*f7p**77*f9p**103 * (f2p**181*f4p**65*f5p**445*f8p**276).frobenius(3)
    ff = fR0*fR1
    return ff

def FinalExp_hard_BW6_761_multi_2NAF(m,u0):
    """ returns m^exponent where exponent is a multiple of (q^2-q+1)/r"""
    f0 = m
    f1 = m**u0
    f2 = f1**u0
    f3 = f2**u0
    f4 = f3**u0
    f5 = f4**u0
    f6 = f5**u0
    f7 = f6**u0
    f0p = m.frobenius()
    f1p = f1.frobenius()
    f2p = f2.frobenius()
    f3p = f3.frobenius()
    f4p = f4.frobenius()
    f5p = f5.frobenius()
    f6p = f6.frobenius()
    f7p = f7.frobenius()
    f8p = f7p**u0
    f9p = f8p**u0

    f = f3p*f6p*(f5p).frobenius(3)                         # 2M
    f = f**2
    f4f2p = f4*f2p                                         # 1M
    f *= f5*f0p*(f0*f1*f3*f4f2p*f8p).frobenius(3)          # 7M
    f = f**2
    f *= f9p*(f7).frobenius(3)                             # 2M
    f = f**2
    f2f4p = f2*f4p                                         # 1M
    f4f2pf5p = f4f2p*f5p                                   # 1M
    f *= f4f2pf5p*f6*f7p*(f2f4p*f3*f3p).frobenius(3)       # 6M
    f = f**2
    f *= f0*f7*f1p*(f0p*f9p).frobenius(3)                  # 5M
    f = f**2
    f6pf8p = f6p*f8p                                       # 1M
    f5f7p = f5*f7p                                         # 1M
    f *= f5f7p*f2p*(f6pf8p).frobenius(3)                   # 3M
    f = f**2
    f3f6 = f3*f6                                           # 1M
    f1f7 = f1*f7                                           # 1M
    f *= f3f6*f9p*(f1f7*f2).frobenius(3)                   # 4M
    f = f**2
    f *= f0*f0p*f3p*f5p*(f4f2p*f5f7p*f6pf8p).frobenius(3)  # 7M
    f = f**2
    f *= f1p*(f3f6).frobenius(3)                           # 2M
    f = f**2
    f *= f1f7*f5f7p*f0p*(f2f4p*f4f2pf5p*f9p).frobenius(3)  # 6M
    # 51 M
    #  9 S
    return f

def FinalExp_BW6_761(m,u0):
    f = FinalExp_easy_BW6_761(m)
    #f = FinalExp_hard_BW6_761(f,u0)
    f = FinalExp_hard_BW6_761_multi_2NAF(f,u0)
    return f

def OptimalAtePairing_BW6_761(Q,P,u0):
    m = MillerLoop_BW6_761(Q,P,u0)
    f = FinalExp_BW6_761(m,u0)
    return f

####### membership testing, co-factor multiplication
# Formulas from Fuentes-Castaneda, Knapp, Rodriguez-Henriquez
# Faster hashing to G2.
# In: Miri, Vaudenay (eds.) SAC 2011. LNCS, vol. 7118, pp. 412-430.
# Springer, Heidelberg (Aug 2012). https://doi.org/10.1007/978-3-642-28496-0_25
# Example for BLS12-381: https://eprint.iacr.org/2019/814
# Sean Bowe, Faster Subgroup Checks for BLS12-381

def BW6_761_phi(P):
    # omegx := (103*x^11 - 482*x^10 + 732*x^9 + 62*x^8 - 1249*x^7 + 1041*x^6 + 214*x^5 - 761*x^4 + 576*x^3 + 11*x^2 - 265*x + 66)/21;
    Fp = P[0].parent()
    omega = Fp(0x531DC16C6ECD27AA846C61024E4CCA6C1F31E53BD9603C2D17BE416C5E4426EE4A737F73B6F952AB5E57926FA701848E0A235A0A398300C65759FC45183151F2F082D4DCB5E37CB6290012D96F8819C547BA8A4000002F962140000000002A)
    return P.curve()([omega*P[0], P[1]])

def BW6_761_G1_mult_by_cofactor(P):
    # multiply point P by a multiple of the cofactor c where c*r = #E(Fp)
    # cx = (103*x^6 - 173*x^5 - 96*x^4 + 293*x^3 + 21*x^2 + 52*x + 172)/3
    u0 = 0x8508C00000000001
    xP = u0*P
    x2P = u0*xP
    x3P = u0*x2P
    Q = (7*x2P +89*xP + 130*P)
    phi_Q = BW6_761_phi(Q)
    R = (103*x3P -83*x2P -40*xP + 136*P) + phi_Q
    return R

def BW6_761_G1_mult_by_cofactor_alt(P):
    # multiply point P by a multiple of the cofactor c where c*r = #E(Fp)
    # cx = (103*x^6 - 173*x^5 - 96*x^4 + 293*x^3 + 21*x^2 + 52*x + 172)/3
    # alternative formula
    u0 = 0x8508C00000000001
    xP = u0*P
    x2P = u0*xP
    x3P = u0*x2P
    Q = (103*x3P - 90*x2P - 129*xP + 6*P)
    phi_Q = BW6_761_phi(Q)
    R = -(7*x2P + 89*xP + 130*P) + phi_Q
    return R

def BW6_761_G1_mult_by_r(P):
    # multiply P by a multiple of r in order to be able to check if r*P == 0 in E(Fp)
    u0 = 0x8508C00000000001
    xP = u0*P
    x2P = u0*xP
    x3P = u0*x2P
    Q = (x3P - x2P + P)
    phi_Q = BW6_761_phi(Q)
    R = xP + P + phi_Q
    return R

def BW6_761_G1_mult_by_r_alt(P):
    # multiply P by a multiple of r in order to be able to check if r*P == 0 in E(Fp)
    # alternative formula
    u0 = 0x8508C00000000001
    xP = u0*P
    x2P = u0*xP
    x3P = u0*x2P
    Q = -(xP + P)
    phi_Q = BW6_761_phi(Q)
    R = (x3P - x2P - xP) + phi_Q
    return R

def BW6_761_G1_check_membership(P):
    R = BW6_761_G1_mult_by_r(P)
    return R == P.curve()(0)

def BW6_761_G1_check_membership_alt(P):
    # alternative formula
    R = BW6_761_G1_mult_by_r_alt(P)
    return R == P.curve()(0)

def BW6_761_G2_mult_by_cofactor(Q):
    # multiply point Q by a multiple of the cofactor c' where c'*r = #E'(Fp)
    # cx = (103*x^6 - 173*x^5 - 96*x^4 + 293*x^3 + 21*x^2 + 52*x + 151)/3
    # note that only the constant coefficient of cx differs from cx for G1
    u0 = 0x8508C00000000001
    xQ = u0*Q
    x2Q = u0*xQ
    x3Q = u0*x2Q
    S = (7*x2Q -117*xQ - 109*Q)
    phi_S = BW6_761_phi(S)
    R = (103*x3Q -83*x2Q -143*xQ + 27*Q) + phi_S
    return R

def BW6_761_G2_mult_by_cofactor_alt(Q):
    # multiply point Q by a multiple of the cofactor c' where c'*r = #E'(Fp)
    # cx = (103*x^6 - 173*x^5 - 96*x^4 + 293*x^3 + 21*x^2 + 52*x + 151)/3
    # note that only the constant coefficient of cx differs from cx for G1
    # alternative formula
    u0 = 0x8508C00000000001
    xQ = u0*Q
    x2Q = u0*xQ
    x3Q = u0*x2Q
    S = (103*x3Q - 90*x2Q - 26*xQ + 136*Q)
    phi_S = BW6_761_phi(S)
    R = (-7*x2Q +117*xQ + 109*Q) + phi_S
    return R

def BW6_761_G2_mult_by_r(Q):
    # multiply Q by a multiple of r in order to be able to check if r*Q == 0 in E'(Fp)
    u0 = 0x8508C00000000001
    xQ = u0*Q
    x2Q = u0*xQ
    x3Q = u0*x2Q
    S = (x3Q -x2Q - xQ)
    phi_S = BW6_761_phi(S)
    R = (-xQ - Q) + phi_S
    return R

def BW6_761_G2_mult_by_r_alt(Q):
    # multiply Q by a multiple of r in order to be able to check if r*Q == 0 in E'(Fp)
    # alternative formula
    u0 = 0x8508C00000000001
    xQ = u0*Q
    x2Q = u0*xQ
    x3Q = u0*x2Q
    S = (xQ + Q)
    phi_S = BW6_761_phi(S)
    R = (x3Q - x2Q + Q) + phi_S
    return R

def BW6_761_G2_check_membership(Q):
    # check that r*Q == 0 in E'(Fp)
    R = BW6_761_G2_mult_by_r(Q)
    return R == Q.curve()(0)

def BW6_761_G2_check_membership_alt(Q):
    # check that r*Q == 0 in E'(Fp)
    # alternative formula
    R = BW6_761_G2_mult_by_r_alt(Q)
    return R == Q.curve()(0)

