from pairing import *
from sage.rings.integer_ring import Z, ZZ
from sage.rings.rational_field import Q, QQ
from sage.misc.functional import cyclotomic_polynomial
from sage.rings.finite_rings.finite_field_constructor import FiniteField, GF
from sage.schemes.elliptic_curves.constructor import EllipticCurve

# this is much much faster with this statement:
# proof.arithmetic(False)
from sage.structure.proof.all import arithmetic
arithmetic(False)


# test subgroup membership testing and cofactor multiplication
def test_BW6_761_phi(E,r,c):
    #print("test BW6_761_phi(P) (P in G1)")
    ok = True
    for i in range(10):
        P = E.random_element()
        assert r*c*P == E(0)
        phiP = BW6_761_phi(P)
        phi2P = BW6_761_phi(phiP)
        ok = ok and (phi2P + phiP + P == E(0))
    print("test BW6_761_phi(P) (P in G1): {}".format(ok))
    return ok;

def test_BW6_761_G1_mult_by_cofactor(E,r,c):
    #print("test BW6_761_G1_mult_by_cofactor(P)")
    ok = True
    for i in range(10):
        P = E.random_element()
        rP = r*P
        assert rP != E(0) and c*rP == E(0) and c*P != E(0)
        R = BW6_761_G1_mult_by_cofactor(P)
        ok = ok and (r*R == E(0))
        R = BW6_761_G1_mult_by_cofactor(rP)
        ok = ok and (R == E(0))
    print("test BW6_761_G1_mult_by_cofactor(P): {}".format(ok))
    return ok

def test_BW6_761_G1_mult_by_cofactor_alt(E,r,c):
    #print("test BW6_761_G1_mult_by_cofactor_alt(P)")
    ok = True
    for i in range(10):
        P = E.random_element()
        rP = r*P
        assert rP != E(0) and c*rP == E(0) and c*P != E(0)
        R = BW6_761_G1_mult_by_cofactor_alt(P)
        ok = ok and (r*R == E(0))
        R = BW6_761_G1_mult_by_cofactor_alt(rP)
        ok = ok and (R == E(0))
    print("test BW6_761_G1_mult_by_cofactor_alt(P): {}".format(ok))
    return ok

def test_BW6_761_G1_mult_by_r(E,r,c):
    #print("test BW6_761_G1_mult_by_r(P)")
    ok = True
    for i in range(10):
        P = E.random_element()
        cP = c*P
        assert cP != E(0) and r*cP == E(0) and r*P != E(0)
        R = BW6_761_G1_mult_by_r(P)
        ok = ok and (c*R == E(0))
        R = BW6_761_G1_mult_by_r(cP)
        ok = ok and (R == E(0))
    print("test BW6_761_G1_mult_by_r(P): {}".format(ok))
    return ok

def test_BW6_761_G1_mult_by_r_alt(E,r,c):
    #print("test BW6_761_G1_mult_by_r_alt(P)")
    ok = True
    for i in range(10):
        P = E.random_element()
        cP = c*P
        assert cP != E(0) and r*cP == E(0) and r*P != E(0)
        R = BW6_761_G1_mult_by_r_alt(P)
        ok = ok and (c*R == E(0))
        R = BW6_761_G1_mult_by_r_alt(cP)
        ok = ok and (R == E(0))
    print("test BW6_761_G1_mult_by_r_alt(P): {}".format(ok))
    return ok

def test_BW6_761_G1_check_membership(E,r,c):
    #print("test BW6_761_G1_check_membership(P)")
    ok = True
    for i in range(10):
        P = E.random_element()
        rP = r*P
        cP = c*P
        assert rP != E(0) and cP != E(0) and c*rP == E(0)
        val = BW6_761_G1_check_membership(P)
        ok = ok and not val
        val = BW6_761_G1_check_membership(rP)
        ok = ok and not val
        val = BW6_761_G1_check_membership(cP)
        ok = ok and val
        C = BW6_761_G1_mult_by_cofactor(P)
        val = BW6_761_G1_check_membership(C)
        ok = ok and val
    print("test BW6_761_G1_check_membership(P): {}".format(ok))
    return ok

def test_BW6_761_G1_check_membership_alt(E,r,c):
    #print("test BW6_761_G1_check_membership_alt(P)")
    ok = True
    for i in range(10):
        P = E.random_element()
        cP = c*P
        rP = r*P
        assert rP != E(0) and cP != E(0) and c*rP == E(0)
        val = BW6_761_G1_check_membership_alt(P)
        ok = ok and not val
        val = BW6_761_G1_check_membership_alt(rP)
        ok = ok and not val
        val = BW6_761_G1_check_membership_alt(cP)
        ok = ok and val
        C = BW6_761_G1_mult_by_cofactor_alt(P)
        val = BW6_761_G1_check_membership_alt(C)
        ok = ok and val
    print("test BW6_761_G1_check_membership_alt(P): {}".format(ok))
    return ok

# test subgroup membership testing and cofactor multiplication
def test_BW6_761_phi_G2(E2,r,c2):
    #print("test BW6_761_phi(Q) (Q in G2)")
    ok = True
    for i in range(10):
        Q = E2.random_element()
        assert r*c2*Q == E2(0)
        phiQ = BW6_761_phi(Q)
        phi2Q = BW6_761_phi(phiQ)
        ok = ok and (phi2Q + phiQ + Q == E2(0))
    print("test BW6_761_phi(Q) (Q in G2): {}".format(ok))
    return ok

def test_BW6_761_G2_mult_by_cofactor(E2,r,c2):
    #print("test BW6_761_G2_mult_by_cofactor(Q)")
    ok = True
    for i in range(10):
        Q = E2.random_element()
        C = r*Q
        assert C != E2(0) and c2*Q != E2(0) and c2*C == E2(0)
        R = BW6_761_G2_mult_by_cofactor(Q)
        ok = ok and (r*R == E2(0))
        R = BW6_761_G2_mult_by_cofactor(C)
        ok = ok and (R == E2(0))
    print("test BW6_761_G2_mult_by_cofactor(Q): {}".format(ok))
    return ok

def test_BW6_761_G2_mult_by_cofactor_alt(E2,r,c2):
    #print("test BW6_761_G2_mult_by_cofactor_alt(Q)")
    ok = True
    for i in range(10):
        Q = E2.random_element()
        C = r*Q
        assert C != E2(0) and c2*Q != E2(0) and c2*C == E2(0)
        R = BW6_761_G2_mult_by_cofactor_alt(Q)
        ok = ok and (r*R == E2(0))
        R = BW6_761_G2_mult_by_cofactor(C)
        ok = ok and (R == E2(0))
    print("test BW6_761_G2_mult_by_cofactor_alt(Q): {}".format(ok))
    return ok

def test_BW6_761_G2_mult_by_r(E2,r,c2):
    #print("test BW6_761_G2_mult_by_r(Q)")
    ok = True
    for i in range(10):
        Q = E2.random_element()
        C = c2*Q
        assert C != E2(0) and r*Q != E2(0) and r*C == E2(0)
        R = BW6_761_G2_mult_by_r(Q)
        ok = ok and (c2*R == E2(0))
        R = BW6_761_G2_mult_by_r(C)
        ok = ok and (R == E2(0))
    print("test BW6_761_G2_mult_by_r(Q): {}".format(ok))
    return ok

def test_BW6_761_G2_mult_by_r_alt(E2,r,c2):
    #print("test BW6_761_G2_mult_by_r_alt(Q)")
    ok = True
    for i in range(10):
        Q = E2.random_element()
        C = c2*Q
        assert C != E2(0) and r*Q != E2(0) and r*C == E2(0)
        R = BW6_761_G2_mult_by_r_alt(Q)
        ok = ok and (c2*R == E2(0))
        R = BW6_761_G2_mult_by_r(C)
        ok = ok and (R == E2(0))
    print("test BW6_761_G2_mult_by_r_alt(Q): {}".format(ok))
    return ok

def test_BW6_761_G2_check_membership(E2,r,c2):
    #print("test BW6_761_G2_check_membership(Q)")
    ok = True
    for i in range(10):
        Q = E2.random_element()
        rQ = r*Q
        c2Q = c2*Q
        assert rQ != E2(0) and c2Q != E2(0) and c2*rQ == E2(0)
        val = BW6_761_G2_check_membership(Q)
        ok = ok and not val
        val = BW6_761_G2_check_membership(rQ)
        ok = ok and not val
        val = BW6_761_G2_check_membership(c2Q)
        ok = ok and val
        C = BW6_761_G2_mult_by_cofactor(Q)
        val = BW6_761_G2_check_membership(C)
        ok = ok and val
    print("test BW6_761_G2_check_membership(Q): {}".format(ok))
    return ok

def test_BW6_761_G2_check_membership_alt(E2,r,c2):
    #print("test BW6_761_G2_check_membership_alt(Q)")
    ok = True
    for i in range(10):
        Q = E2.random_element()
        rQ = r*Q
        c2Q = c2*Q
        assert rQ != E2(0) and c2Q != E2(0) and c2*rQ == E2(0)
        val = BW6_761_G2_check_membership_alt(Q)
        ok = ok and not val
        val = BW6_761_G2_check_membership_alt(rQ)
        ok = ok and not val
        val = BW6_761_G2_check_membership_alt(c2Q)
        ok = ok and val
        C = BW6_761_G2_mult_by_cofactor_alt(Q)
        val = BW6_761_G2_check_membership_alt(C)
        ok = ok and val
    print("test BW6_761_G2_check_membership(Q): {}".format(ok))
    return ok

def test_G2_Frobenius_eigenvalue_MD_twist(E_Fqk,E2,r,c2,D_twist=False):
    ok = True
    Fqk = E_Fqk.base_field()
    w = Fqk.gen(0)
    q = Fqk.characteristic()
    for i in range(10):
        Q2 = c2 * E2.random_element()
        while Q2 == E2(0):
            Q2 = c2 * E2.random_element()
        assert r*Q2 == E2(0)
        if D_twist:
            Q = E_Fqk(psi_sextic_D_twist(Q2, w))
        else:
            Q = E_Fqk(psi_sextic_M_twist(Q2, w))
        piQ = E_Fqk([(Q[0]).frobenius(), (Q[1]).frobenius()])
        ok = ok and piQ == q*Q
    print("test Frobenius(Q) == q*Q: {}".format(ok))
    return ok

def test_bilinear_MillerLoop_BW6_761_naive_MD_twist(E,E_Fqk,E2,r,c,c2,u0,D_twist=False):
    # define two base-points P and Q for pairing tests
    Fqk = E_Fqk.base_field()
    k = Fqk.degree()
    w = Fqk.gen(0)
    q = Fqk.characteristic()
    P = c * E.random_element()
    while P == E(0):
        P = c * E.random_element()
    Q2 = c2*E2.random_element()
    while Q2 == E2(0):
        Q2 = c2 * E2.random_element()
    if D_twist:
        Q = E_Fqk(psi_sextic_D_twist(Q2, w))
    else:
        Q = E_Fqk(psi_sextic_M_twist(Q2, w))
    f = MillerLoop_BW6_761_naive(Q,P,u0)
    exponent = (q**k-1) // r
    g = f**exponent

    ok = True
    for aa in range(1,4):
        for bb in range(1,4):
	    fij = MillerLoop_BW6_761_naive(bb*Q,aa*P,u0)
            gij = fij**exponent
	    ok = ok and gij == g**(aa*bb)
    print("test bilinear MillerLoop_BW6_761_naive: {}".format(ok))
    return ok

def test_bilinear_MillerLoop_BW6_761_MD_twist(E,E_Fqk,E2,r,c,c2,u0,D_twist=False):
    # define two base-points P and Q for pairing tests
    Fqk = E_Fqk.base_field()
    k = Fqk.degree()
    w = Fqk.gen(0)
    q = Fqk.characteristic()
    P = c * E.random_element()
    while P == E(0):
        P = c * E.random_element()
    Q2 = c2*E2.random_element()
    while Q2 == E2(0):
        Q2 = c2 * E2.random_element()
    if D_twist:
        Q = E_Fqk(psi_sextic_D_twist(Q2, w))
    else:
        Q = E_Fqk(psi_sextic_M_twist(Q2, w))
    f = MillerLoop_BW6_761(Q,P,u0)
    exponent = (q**k-1) // r
    g = f**exponent

    ok = True
    for aa in range(1,4):
        for bb in range(1,4):
	    fij = MillerLoop_BW6_761(bb*Q,aa*P,u0)
            gij = fij**exponent
	    ok = ok and gij == g**(aa*bb)
    print("test bilinear MillerLoop_BW6_761: {}".format(ok))
    return ok

def test_bilinear_MillerLoop_2NAF_BW6_761_MD_twist(E,E_Fqk,E2,r,c,c2,u0,D_twist=False):
    # define two base-points P and Q for pairing tests
    Fqk = E_Fqk.base_field()
    k = Fqk.degree()
    w = Fqk.gen(0)
    q = Fqk.characteristic()
    P = c * E.random_element()
    while P == E(0):
        P = c * E.random_element()
    Q2 = c2*E2.random_element()
    while Q2 == E2(0):
        Q2 = c2 * E2.random_element()
    if D_twist:
        Q = E_Fqk(psi_sextic_D_twist(Q2, w))
    else:
        Q = E_Fqk(psi_sextic_M_twist(Q2, w))
    f = MillerLoop_2NAF_BW6_761(Q,P,u0)
    exponent = (q**k-1) // r
    g = f**exponent

    ok = True
    for aa in range(1,4):
        for bb in range(1,4):
	    fij = MillerLoop_2NAF_BW6_761(bb*Q,aa*P,u0)
            gij = fij**exponent
	    ok = ok and gij == g**(aa*bb)
    print("test bilinear MillerLoop_2NAF_BW6_761: {}".format(ok))
    return ok

def test_MillerLoop_ate_all_BW6_761_MD_twist(E,E_Fqk,E2,r,c,c2,u0,D_twist=False):
    #Fqk.cardinality() == q^k
    Fqk = E_Fqk.base_field()
    w = Fqk.gen(0)
    exponent = (Fqk.cardinality()-1) // r
    ok = True
    for i in range(10):
        P = c*E.random_element()
        while P == E(0):
            P = c*E.random_element()
        Q2 = c2*E2.random_element()
        while Q2 == E2(0):
            Q2 = c2 * E2.random_element()
        if D_twist:
            Q = E_Fqk(psi_sextic_D_twist(Q2, w))
        else:
            Q = E_Fqk(psi_sextic_M_twist(Q2, w))
        
        m0 = MillerLoop_BW6_761_naive(Q,P,u0)
        m1 = MillerLoop_BW6_761(Q,P,u0)
        m2 = MillerLoop_2NAF_BW6_761(Q,P,u0)
        f0 = m0**exponent
        f1 = m1**exponent
        f2 = m2**exponent
	ok = ok and f0 == f1 and f1 == f2
    print("test all MillerLoop_BW6_761 (naive,default,2NAF): {}".format(ok))
    return ok

def test_FinalExp_BW6_761(Fqk,r,u0,expected_exponent=None):
    #k = Fqk.degree()
    #q = Fqk.characteristic()
    #Fqk.cardinality() == q^k
    #exponent = (Fqk.cardinality()-1) // r
    # actually this function uses a multiple of this exponent
    ok = True
    for i in range(10):
        f = Fqk.random_element()
        g = FinalExp_BW6_761(f,u0)
        ok = ok and g**r == Fqk(1)
        if expected_exponent != None and expected_exponent != 0:
            ok = ok and g == f**expected_exponent
    print("test FinalExp_BW6_761: {}".format(ok))
    return ok

def test_OptimalAtePairing_BW6_761_MD_twist(E,E_Fqk,E2,r,c,c2,u0,D_twist=False):
    Fqk = E_Fqk.base_field()
    k = Fqk.degree()
    w = Fqk.gen(0)
    q = Fqk.characteristic()
    P = c * E.random_element()
    while P == E(0):
        P = c * E.random_element()
    Q2 = c2*E2.random_element()
    while Q2 == E2(0):
        Q2 = c2 * E2.random_element()
    if D_twist:
        Q = E_Fqk(psi_sextic_D_twist(Q2, w))
    else:
        Q = E_Fqk(psi_sextic_M_twist(Q2, w))
    f = OptimalAtePairing_BW6_761(Q,P,u0)

    ok = True
    for aa in range(1,4):
        for bb in range(1,4):
            fij = OptimalAtePairing_BW6_761(bb*Q,aa*P,u0)
	    ok = ok and fij == f**(aa*bb)
    print("test bilinear OptimalAtePairing_BW6_761: {}".format(ok))
    return ok

def test_double_line_H_a0_twist6_AKLGL(E,E2,Fqk,r,c,c2,D_twist=False,verbose=False):
    if verbose:
        print("test double_line_H_a0_twist6_AKLGL")
    w = Fqk.gen(0)
    Q1 = c2*E2.random_element()
    assert r*Q1 == E2(0)
    P = c*E.random_element()
    Pxy = (P[0],P[1])
    Q2 = 2*Q1
    Q3 = 3*Q1
    Q1xy = (Q1[0],Q1[1])
    Sxy = (Q1[0],Q1[1],1) # with Z=1
    ln, S2 = double_line_H_a0_twist6_AKLGL(Sxy,Pxy,E2.a6(),D_twist=D_twist)
    ok0 = S2[0]/S2[2] == Q2[0] and S2[1]/S2[2] == Q2[1]
    lln,SS2,lxy = double_line_AKLGL_test(Sxy,Pxy,E2.a6(),Fqk.gen(0),D_twist=D_twist)
    ok1 = SS2[0]/SS2[2] == Q2[0] and SS2[1]/SS2[2] == Q2[1]
    ok2 = Fqk(ln) == lln
    if verbose:
        print("S2 == 2*Q: {}".format(ok0))
        print("SS2== 2*Q: {}".format(ok1))
        print("test lines: {}".format(ok2))
    l0,lx,ly = lxy
    if D_twist:
        okl1 = l0 + lx*Q1[0]*w**2 + ly*Q1[1]*w**3 == 0
        okl2 = l0 + lx*Q2[0]*w**2 - ly*Q2[1]*w**3 == 0
    else:
        okl1 = l0 + lx*Q1[0]/w**2 + ly*Q1[1]/w**3 == 0
        okl2 = l0 + lx*Q2[0]/w**2 - ly*Q2[1]/w**3 == 0
    if verbose:
        print("l(xQ,yQ)   ==0: {}".format(okl1))
        print("l(x2Q,-y2Q)==0: {}".format(okl2))
    ok = ok0 and ok1 and ok2 and okl1 and okl2
    print("test double_line_H_a0_twist6_AKLGL: {}".format(ok))
    return ok

def test_add_line_H_a0_twist6_AKLGL(E,E2,Fqk,r,c,c2,D_twist=False,verbose=False):
    if verbose:
        print("test add_line_H_a0_twist6_AKLGL")
    w = Fqk.gen(0)
    Q1 = c2*E2.random_element()
    assert r*Q1 == E2(0)
    P = c*E.random_element()
    Pxy = (P[0],P[1])
    Q2 = 2*Q1
    Q3 = 3*Q1
    Q1xy = (Q1[0],Q1[1])
    S2 = (Q2[0],Q2[1],1) # with Z=1
    ln,SQ = add_line_H_a0_twist6_AKLGL(S2,Q1xy,Pxy,D_twist=D_twist)
    ok0 = SQ[0]/SQ[2] ==  Q3[0] and SQ[1]/SQ[2] ==  Q3[1]
    if verbose:
        print("SQ == 3*Q: {}".format(ok0))
    if D_twist:
        l0,lx,ly = ln[3]*w**3,ln[1]/P[0]*w,ln[0]/P[1]
        ok1 = l0 + lx*Q1[0]*w**2 + ly*Q1[1]*w**3 == 0
        ok2 = l0 + lx*Q2[0]*w**2 + ly*Q2[1]*w**3 == 0
        ok3 = l0 + lx*Q3[0]*w**2 - ly*Q3[1]*w**3 == 0
    else:
        l0,lx,ly = ln[0],ln[2]/P[0]*w**2,ln[3]/P[1]*w**3
        ok1 = l0 + lx*Q1[0]/w**2 + ly*Q1[1]/w**3 == 0
        ok2 = l0 + lx*Q2[0]/w**2 + ly*Q2[1]/w**3 == 0
        ok3 = l0 + lx*Q3[0]/w**2 - ly*Q3[1]/w**3 == 0
    if verbose:
        print("l(xQ,yQ)    ==0: {}".format(ok1))
        print("l(x2Q,y2Q)  ==0: {}".format(ok2))
        print("l(x3Q,-y3Q) ==0: {}".format(ok3))
    #print("l0={}\nlx={}\nly={}".format(l0,lx,ly))
    lln,SSQ,lxy_ = add_line_AKLGL_test(S2,Q1xy,Pxy,w,D_twist=D_twist)
    ok4 = SSQ[0]/SSQ[2] ==  Q3[0] and SSQ[1]/SSQ[2] ==  Q3[1]
    ok5 = Fqk(ln) == lln
    if verbose:
        print("add_line_AKLGL_test(S2,Q,P,w)")
        print("QSQ== 3*Q: {}".format(ok4))
        print("test lines: {}".format(ok5))
    l0_,lx_,ly_ = lxy_
    if D_twist:
        okl1 = l0_ + lx_*Q1[0]*w**2 + ly_*Q1[1]*w**3 == 0
        okl2 = l0_ + lx_*Q2[0]*w**2 + ly_*Q2[1]*w**3 == 0
        okl3 = l0_ + lx_*Q3[0]*w**2 - ly_*Q3[1]*w**3 == 0
    else:
        okl1 = l0_ + lx_*Q1[0]/w**2 + ly_*Q1[1]/w**3 == 0
        okl2 = l0_ + lx_*Q2[0]/w**2 + ly_*Q2[1]/w**3 == 0
        okl3 = l0_ + lx_*Q3[0]/w**2 - ly_*Q3[1]/w**3 == 0
    if verbose:
        print("l(xQ,yQ)    ==0: {}".format(okl1))
        print("l(x2Q,y2Q)  ==0: {}".format(okl2))
        print("l(x3Q,-y3Q) ==0: {}".format(okl3))
    #print("l0={}\nlx={}\nly={}".format(l0_,lx_,ly_))
    ok = ok0 and ok1 and ok2 and ok3 and ok4 and ok5 and okl1 and okl2 and okl3
    print("test add_line_H_a0_twist6_AKLGL: {}".format(ok))
    return ok

def test_sparse_mult_M6_twist(Fq6):
    Fq = Fq6.base_ring()
    P = Fq6.polynomial()
    xi = -P.constant_coefficient()
    assert P.degree() == 6
    coeffs = P.list()
    assert coeffs[1] == coeffs[2] == coeffs[3] == coeffs[4] == coeffs[5] == 0
    assert coeffs[6] == 1
    assert coeffs[0] == -xi
    ok = True
    for i in range(10):
        f = Fq6.random_element()
        l0 = Fq.random_element()
        l2 = Fq.random_element()
        l3 = Fq.random_element()
        l = Fq6([l0,0,l2,l3,0,0])
        expected_res = l*f
        res = sparse_mult_M6_twist(l0,l2,l3, f, xi, Fq6)
        ok = res == expected_res
    print("test sparse_mult_M6_twist: {}".format(ok))
    return ok

def test_sparse_mult_D6_twist(Fq6):
    Fq = Fq6.base_ring()
    P = Fq6.polynomial()
    xi = -P.constant_coefficient()
    assert P.degree() == 6
    coeffs = P.list()
    assert coeffs[1] == coeffs[2] == coeffs[3] == coeffs[4] == coeffs[5] == 0
    assert coeffs[6] == 1
    assert coeffs[0] == -xi
    ok = True
    for i in range(10):
        f = Fq6.random_element()
        l0 = Fq.random_element()
        l1 = Fq.random_element()
        l3 = Fq.random_element()
        l = Fq6([l0,l1,0,l3,0,0])
        expected_res = l*f
        res = sparse_mult_D6_twist(l0,l1,l3, f, xi, Fq6)
        ok = res == expected_res
    print("test sparse_mult_D6_twist: {}".format(ok))
    return ok

def test_MillerFunction_ate_AKLGL(E,E2,Fqk,r,c,c2,u0,D_twist=False):
    Q1 = c2*E2.random_element()
    assert r*Q1 == E2(0)
    P = c*E.random_element()
    assert r*P == E(0)
    Pxy = (P[0],P[1])
    Q1xy = (Q1[0],Q1[1])
    u = u0+1
    mu, Su = MillerFunction_ate_AKLGL(Q1xy,Pxy,E2.a6(),u,Fqk,m0=1,D_twist=D_twist)
    Qu1 = (u0+1)*E2(Q1)
    ok1 = Su[0]/Su[2] == Qu1[0] and Su[1]/Su[2] == Qu1[1]
    #print("S == (u+1)*Q: {}".format(ok1))
    v = u0*(u0**2-u0-1)
    mv, Sv = MillerFunction_ate_AKLGL(Q1xy,Pxy,E2.a6(),v,Fqk,m0=1,D_twist=D_twist)
    Qv = v*E2(Q1)
    ok2 = Sv[0]/Sv[2] == Qv[0] and Sv[1]/Sv[2] == Qv[1]
    #print("S == v*Q: {}".format(ok2))
    ok = ok1 and ok2
    print("test MillerFunction_ate_AKLGL: {}".format(ok))
    return ok

def test_MillerLoop_ate_AKLGL_naive_MD_twist(E,E2,Fqk,r,c,c2,u0,D_twist=False):
    #if not D_twist: print("test MillerLoop_ate_AKLGL_naive with M-twist")
    #else:           print("test MillerLoop_ate_AKLGL_naive with D-twist")
    P = c*E.random_element()
    while P == E(0):
        P = c*E.random_element()
    Q2 = c2*E2.random_element()
    while Q2 == E2(0):
        Q2 = c2 * E2.random_element()
    #Fqk.cardinality() == q^k
    exponent = (Fqk.cardinality()-1) // r
    m = MillerLoop_ate_AKLGL_naive(Q2,P,E2.a6(),u0,Fqk,D_twist=D_twist)
    f = m**exponent
    ok = True
    for aa in range(1,4):
        for bb in range(1,4):
            mij = MillerLoop_ate_AKLGL_naive(bb*Q2,aa*P,E2.a6(),u0,Fqk,D_twist=D_twist)
            fij = mij**exponent
	    ok = ok and fij == f**(aa*bb)
    print("test bilinear MillerLoop_ate_AKLGL_naive: {}".format(ok))
    return ok

def test_MillerLoop_ate_AKLGL_MD_twist(E,E2,Fqk,r,c,c2,u0,D_twist=False):
    #if not D_twist: print("test MillerLoop_ate_AKLGL with M-twist")
    #else:           print("test MillerLoop_ate_AKLGL with D-twist")
    P = c*E.random_element()
    while P == E(0):
        P = c*E.random_element()
    Q2 = c2*E2.random_element()
    while Q2 == E2(0):
        Q2 = c2 * E2.random_element()
    #Fqk.cardinality() == q^k
    exponent = (Fqk.cardinality()-1) // r
    m = MillerLoop_ate_AKLGL(Q2,P,E2.a6(),u0,Fqk,D_twist=D_twist)
    f = m**exponent
    ok = True
    for aa in range(1,4):
        for bb in range(1,4):
            mij = MillerLoop_ate_AKLGL(bb*Q2,aa*P,E2.a6(),u0,Fqk,D_twist=D_twist)
            fij = mij**exponent
	    ok = ok and fij == f**(aa*bb)
    print("test bilinear MillerLoop_ate_AKLGL: {}".format(ok))
    return ok

def test_MillerLoop_ate_2NAF_AKLGL_MD_twist(E,E2,Fqk,r,c,c2,u0,D_twist=False):
    #if not D_twist: print("test MillerLoop_ate_2NAF_AKLGL with M-twist")
    #else:           print("test MillerLoop_ate_2NAF_AKLGL with D-twist")
    P = c*E.random_element()
    while P == E(0):
        P = c*E.random_element()
    Q2 = c2*E2.random_element()
    while Q2 == E2(0):
        Q2 = c2 * E2.random_element()
    #Fqk.cardinality() == q^k
    exponent = (Fqk.cardinality()-1) // r
    m = MillerLoop_ate_2NAF_AKLGL(Q2,P,E2.a6(),u0,Fqk,D_twist=D_twist)
    f = m**exponent
    ok = True
    for aa in range(1,4):
        for bb in range(1,4):
            mij = MillerLoop_ate_2NAF_AKLGL(bb*Q2,aa*P,E2.a6(),u0,Fqk,D_twist=D_twist)
            fij = mij**exponent
	    ok = ok and fij == f**(aa*bb)
    print("test bilinear MillerLoop_ate_2NAF_AKLGL: {}".format(ok))
    return ok

def test_MillerLoop_ate_all_AKLGL_MD_twist(E,E2,Fqk,r,c,c2,u0,D_twist=False):
    #Fqk.cardinality() == q^k
    exponent = (Fqk.cardinality()-1) // r
    ok = True
    for i in range(10):
        P = c*E.random_element()
        while P == E(0):
            P = c*E.random_element()
        Q2 = c2*E2.random_element()
        while Q2 == E2(0):
            Q2 = c2 * E2.random_element()
        m0 = MillerLoop_ate_AKLGL_naive(Q2,P,E2.a6(),u0,Fqk,D_twist=D_twist)
        m1 = MillerLoop_ate_AKLGL(Q2,P,E2.a6(),u0,Fqk,D_twist=D_twist)
        m2 = MillerLoop_ate_2NAF_AKLGL(Q2,P,E2.a6(),u0,Fqk,D_twist=D_twist)
        f0 = m0**exponent
        f1 = m1**exponent
        f2 = m2**exponent
	ok = ok and f0 == f1 and f1 == f2
    print("test all MillerLoop_ate_AKLGL (naive,default,2NAF): {}".format(ok))
    return ok

def test_MillerLoop_all_BW6_761_and_AKLGL_MD_twist(E,E_Fqk,E2,r,c,c2,u0,D_twist=False):
    Fqk = E_Fqk.base_field()
    w = Fqk.gen(0)
    #Fqk.cardinality() == q^k
    exponent = (Fqk.cardinality()-1) // r
    ok = True
    for i in range(10):
        P = c*E.random_element()
        while P == E(0):
            P = c*E.random_element()
        Q2 = c2*E2.random_element()
        while Q2 == E2(0):
            Q2 = c2 * E2.random_element()
        if D_twist:
            Q = E_Fqk(psi_sextic_D_twist(Q2, w))
        else:
            Q = E_Fqk(psi_sextic_M_twist(Q2, w))
        
        m0 = MillerLoop_BW6_761_naive(Q,P,u0)
        m1 = MillerLoop_BW6_761(Q,P,u0)
        m2 = MillerLoop_2NAF_BW6_761(Q,P,u0)
        m3 = MillerLoop_ate_AKLGL_naive(Q2,P,E2.a6(),u0,Fqk,D_twist=D_twist)
        m4 = MillerLoop_ate_AKLGL(Q2,P,E2.a6(),u0,Fqk,D_twist=D_twist)
        m5 = MillerLoop_ate_2NAF_AKLGL(Q2,P,E2.a6(),u0,Fqk,D_twist=D_twist)
        f0 = m0**exponent
        f1 = m1**exponent
        f2 = m2**exponent
        f3 = m3**exponent
        f4 = m4**exponent
        f5 = m5**exponent
	ok = ok and f0 == f1 and f1 == f2 and f2 == f3 and f3 == f4 and f4 == f5
    print("test all MillerLoop_ate_AKLGL and BW6_761 (naive,default,2NAF): {}".format(ok))
    return ok

def cost_pairing_BW6_761():
    # cost
    # a=0
    # AKLGL'11
    u0 = ZZ(0x8508C00000000001)
    m = 1
    s = 1
    m3 = 6*m
    s3 = 5*m
    m6 = 18*m
    s6 = 12*m
    f6 = 4*m
    i = 25*m
    i6 = 34*m+i
    k = 6
    DoubleLine_ate = 3*m+6*s+(k//3)*m
    AddLine_ate    = 11*m+2*s+(k//3)*m
    Update1        = 13*m+s6
    Update2        = 13*m
    
    HW2NAF_u = sum([1 for bi in bits_2naf(u0) if bi != 0])
    cost_ate1 = (u0.nbits()-1)*DoubleLine_ate + (u0.nbits()-2)*Update1 + (HW2NAF_u-1)*(AddLine_ate+Update2)
    v = u0**2-u0-1
    HW2NAF_v = sum([1 for bi in bits_2naf(v) if bi != 0])
    cost_ate2 = (v.nbits()-1)*(DoubleLine_ate + Update1) + (HW2NAF_v-1)*(AddLine_ate+Update2+m6)
    cost_ate = cost_ate1 + i+2*m+i6+AddLine_ate+Update2 + cost_ate2 + f6 + m6
    
    print("cost ate Miller = {}m".format(cost_ate))
    print("({0}-1)*(3*m+6*s+(k//3)*m) + ({0}-2)*(13*m+s6) + ({1}-1)*(11*m+2*s+(k//3)*m+13*m)".format(u0.nbits(),HW2NAF_u))
    print("+ ({}-1)*(3*m+6*s+(k//3)*m+13*m+s6) + ({}-1)*(11*m+2*s+(k//3)*m+13*m+m6)".format(v.nbits(),HW2NAF_v))
    print("+ i+2*m+i6+13*m+f6+m6")

# even faster exponentiation
def script_fast_final_exponentiation():
    ZZx = ZZ['x']; (x,) = ZZx._first_ngens(1)
    R0 = -103*x**7+70*x**6+269*x**5-197*x**4-314*x**3-73*x**2-263*x-220
    R1 = 103*x**9-276*x**8+77*x**7+492*x**6-445*x**5-65*x**4+452*x**3-181*x**2+34*x+229
    
    for ri in (R0.list() + R1.list()):
        if ri < 0:
            print("{}".format([-ei for ei in bits_2naf(-ri)]))
        else:
            print bits_2naf(ri)
    
    j=0; j0=True
    HW = 0
    for ri in (R0.list() + R1.list()):
        ri = int(ri)
        if ri < 0:
            C = [-ei for ei in bits_2naf(-ri)]
            HW += sum([1 for ei in C if ei != 0])
            C += [0 for i in range(len(C)-1,9)]
            C.reverse()
            s = ""
            for ci in C:
                s += "{: 2d}".format(ci)
            print("{} {} {: 3d}".format(j,s,ri))
        else:
            C = bits_2naf(ri)
            HW += sum([1 for ei in C if ei != 0])
            C += [0 for i in range(len(C)-1,9)]
            C.reverse()
            s = ""
            for ci in C:
                s += "{: 2d}".format(ci)
            print("{} {} {: 3d}".format(j,s,ri))
        j += 1
        if j == len(R0.list()) and j0:
            j = 0
            j0 = False
    print("HW = {}".format(HW))

    j=0; j0=True
    HW = 0
    for ri in (R0.list() + R1.list()):
        ri = ZZ(ri)
        if ri < 0:
            C = [-ei for ei in (-ri).digits(2)]
            HW += sum([1 for ei in C if ei != 0])
            C += [0 for i in range(len(C)-1,9)]
            C.reverse()
            s = ""
            for ci in C:
                s += "{: 2d}".format(ci)
            print("{} {} {: 3d}".format(j,s,ri))
        else:
            C = (ri).digits(2)
            HW += sum([1 for ei in C if ei != 0])
            C += [0 for i in range(len(C)-1,9)]
            C.reverse()
            s = ""
            for ci in C:
                s += "{: 2d}".format(ci)
            print("{} {} {: 3d}".format(j,s,ri))
        j += 1
        if j == len(R0.list()) and j0:
            j = 0
            j0 = False
    print("HW = {}".format(HW))


"""
0  0-1 0 0 1 0 0 1 0 0 -220
1  0-1 0 0 0 0-1 0 0 1 -263
2  0 0 0-1 0 0-1 0 0-1 -73
3  0-1 0-1 0 0 1 0-1 0 -314
4  0-1 0 1 0 0 0-1 0-1 -197
5  0 1 0 0 0 1 0-1 0 1  269
6  0 0 0 1 0 0 1 0-1 0  70
7  0 0-1 0 1 0-1 0 0 1 -103
0  0 1 0 0-1 0 0 1 0 1  229
1  0 0 0 0 1 0 0 0 1 0  34
2  0-1 0 1 0 1 0-1 0-1 -181
3  1 0 0-1 0 0 0 1 0 0  452
4  0 0 0-1 0 0 0 0 0-1 -65
5 -1 0 0 1 0 0 0 1 0-1 -445
6  1 0 0 0 0-1 0-1 0 0  492
7  0 0 0 1 0 1 0-1 0 1  77
8  0-1 0 0 0-1 0-1 0 0 -276
9  0 0 1 0-1 0 1 0 0-1  103
HW = 62
0  0 0-1-1 0-1-1-1 0 0 -220
1  0-1 0 0 0 0 0-1-1-1 -263
2  0 0 0-1 0 0-1 0 0-1 -73
3  0-1 0 0-1-1-1 0-1 0 -314
4  0 0-1-1 0 0 0-1 0-1 -197
5  0 1 0 0 0 0 1 1 0 1  269
6  0 0 0 1 0 0 0 1 1 0  70
7  0 0 0-1-1 0 0-1-1-1 -103
0  0 0 1 1 1 0 0 1 0 1  229
1  0 0 0 0 1 0 0 0 1 0  34
2  0 0-1 0-1-1 0-1 0-1 -181
3  0 1 1 1 0 0 0 1 0 0  452
4  0 0 0-1 0 0 0 0 0-1 -65
5  0-1-1 0-1-1-1-1 0-1 -445
6  0 1 1 1 1 0 1 1 0 0  492
7  0 0 0 1 0 0 1 1 0 1  77
8  0-1 0 0 0-1 0-1 0 0 -276
9  0 0 0 1 1 0 0 1 1 1  103
HW = 76
"""

if __name__ == "__main__":
    #preparse("QQx.<x> = QQ[]")
    QQx = QQ['x']; (x,) = QQx._first_ngens(1)
    u0 = ZZ(0x8508C00000000001)
    #proof.arithmetic(False)
    #SW6-bis parameters
    q = ZZ(6891450384315732539396789682275657542479668912536150109513790160209623422243491736087683183289411687640864567753786613451161759120554247759349511699125301598951605099378508850372543631423596795951899700429969112842764913119068299)
    Fq = GF(q, proof=False)
    #Fqz.<z> = Fq[]
    Fqz = Fq['z']; (z,) = Fqz._first_ngens(1)
    r = ZZ(258664426012969094010652733694893533536393512754914660539884262666720468348340822774968888139573360124440321458177)
    c = ZZ(26642435879335816683987677701488073867751118270052650655942102502312977592501693353047140953112195348280268661194876)
    t = ZZ(3362637538168598222219435186298528655381674028954528064283340709388076588006567983337308081752755143497537638367248)
    y = ZZ(2327979834116721846122857819342346041630394402507777770613906795574054381627779834062290838568927395079900712927242)
    k = 6
    rx = (x**6 - 2*x**5 + 2*x**3 + x + 1)/3
    qx = (103*x**12 - 379*x**11 + 250*x**10 + 691*x**9 - 911*x**8 - 79*x**7 + 623*x**6 - 640*x**5 + 274*x**4 + 763*x**3 + 73*x**2 + 254*x + 229)/9
    tx = (13*x**6 - 23*x**5 - 9*x**4 + 35*x**3 + 10*x + 22)/3
    yx = (9*x**6 - 17*x**5 - 3*x**4 + 21*x**3 + 8*x + 12)/3
    cx = (103*x**6 - 173*x**5 - 96*x**4 + 293*x**3 + 21*x**2 + 52*x + 172)/3
    c2x = (103*x**6 - 173*x**5 - 96*x**4 + 293*x**3 + 21*x**2 + 52*x + 151)/3
    
    q_ = ZZ(qx(u0))
    r_ = ZZ(rx(u0))
    c_ = ZZ(cx(u0))
    t_ = ZZ(tx(u0))
    y_ = ZZ(yx(u0))
    c2_ = ZZ(c2x(u0))
    c2 = c2_
    
    assert t**2 + 3*y**2 == 4*q
    assert t_**2 + 3*y_**2 == 4*q_
    assert q == q_
    assert r == r_
    assert c == c_
    assert t == t_
    assert y == y_
    
    E = EllipticCurve([Fq(0),Fq(-1)])
    b = Fq(-1)
    print(E)
    P = E.random_element()
    assert r*c*P == E(0)
    
    test_BW6_761_phi(E,r,c)
    test_BW6_761_G1_mult_by_cofactor(E,r,c)
    test_BW6_761_G1_mult_by_cofactor_alt(E,r,c)
    test_BW6_761_G1_mult_by_r(E,r,c)
    test_BW6_761_G1_mult_by_r_alt(E,r,c)
    test_BW6_761_G1_check_membership(E,r,c)
    test_BW6_761_G1_check_membership_alt(E,r,c)
    
    # M-twist
    E_M = EllipticCurve([Fq(0),Fq(4)])
    print(E_M)
    QM = E_M.random_element()
    assert c2*r *QM == E_M(0)
    
    print("tests with M-twist")
    test_BW6_761_phi_G2(E_M,r,c2)
    test_BW6_761_G2_mult_by_cofactor(E_M,r,c2)
    test_BW6_761_G2_mult_by_cofactor_alt(E_M,r,c2)
    test_BW6_761_G2_mult_by_r(E_M,r,c2)
    test_BW6_761_G2_mult_by_r_alt(E_M,r,c2)
    test_BW6_761_G2_check_membership(E_M,r,c2)
    test_BW6_761_G2_check_membership_alt(E_M,r,c2)
    
    E_D = EllipticCurve([Fq(0), Fq(-1)/Fq(2)])
    print(E_D)
    QD = E_D.random_element()
    assert c2*r *QD == E_D(0)
    
    print("tests with D-twist")
    test_BW6_761_phi_G2(E_D,r,c2)
    test_BW6_761_G2_mult_by_cofactor(E_D,r,c2)
    test_BW6_761_G2_mult_by_cofactor_alt(E_D,r,c2)
    test_BW6_761_G2_mult_by_r(E_D,r,c2)
    test_BW6_761_G2_mult_by_r_alt(E_D,r,c2)
    test_BW6_761_G2_check_membership(E_D,r,c2)
    test_BW6_761_G2_check_membership_alt(E_D,r,c2)
    
    
    # final exponentiation
    R0 = -103*x**7+70*x**6+269*x**5-197*x**4-314*x**3-73*x**2-263*x-220;
    R1 = 103*x**9-276*x**8+77*x**7+492*x**6-445*x**5-65*x**4+452*x**3-181*x**2+34*x+229;
    
    exponent = cyclotomic_polynomial(6)(qx) // rx
    assert (R0 + R1*qx) == 3*(x**3 - x**2 + 1)*exponent
    e0 = ZZ((((x**k-1) // cyclotomic_polynomial(k))(qx))(u0))
    e1 = ZZ(exponent(u0))
    ee = ZZ((R0 + R1*qx)(u0))
    
    print("test pairings with M-twist")
    #Fq6M.<wM> = Fq.extension(z**6 + 4)
    #preparse("Fq6M.<wM> = Fq.extension(z**6 + 4)")
    Fq6M = Fq.extension(z**6 + 4, names=('wM',),proof=False); (wM,) = Fq6M._first_ngens(1)
    
    E6M = EllipticCurve([Fq6M(0),Fq6M(-1)])
    
    test_G2_Frobenius_eigenvalue_MD_twist(E6M,E_M,r,c2,D_twist=False)
    test_bilinear_MillerLoop_BW6_761_MD_twist(E,E6M,E_M,r,c,c2,u0,D_twist=False)
    test_bilinear_MillerLoop_2NAF_BW6_761_MD_twist(E,E6M,E_M,r,c,c2,u0,D_twist=False)
    test_MillerLoop_ate_all_BW6_761_MD_twist(E,E6M,E_M,r,c,c2,u0,D_twist=False)
    
    test_FinalExp_BW6_761(Fq6M,r,u0)
    test_FinalExp_BW6_761(Fq6M,r,u0,expected_exponent=e0*ee)
    test_OptimalAtePairing_BW6_761_MD_twist(E,E6M,E_M,r,c,c2,u0,D_twist=False)
    
    test_double_line_H_a0_twist6_AKLGL(E,E_M,Fq6M,r,c,c2,D_twist=False,verbose=False)
    test_add_line_H_a0_twist6_AKLGL(E,E_M,Fq6M,r,c,c2,D_twist=False,verbose=False)
    test_sparse_mult_M6_twist(Fq6M)
    
    test_MillerFunction_ate_AKLGL(E,E_M,Fq6M,r,c,c2,u0,D_twist=False)
    
    test_MillerLoop_ate_AKLGL_naive_MD_twist(E,E_M,Fq6M,r,c,c2,u0,D_twist=False)
    test_MillerLoop_ate_AKLGL_MD_twist(E,E_M,Fq6M,r,c,c2,u0,D_twist=False)
    test_MillerLoop_ate_2NAF_AKLGL_MD_twist(E,E_M,Fq6M,r,c,c2,u0,D_twist=False)
    test_MillerLoop_ate_all_AKLGL_MD_twist(E,E_M,Fq6M,r,c,c2,u0,D_twist=False)
    test_MillerLoop_all_BW6_761_and_AKLGL_MD_twist(E,E6M,E_M,r,c,c2,u0,D_twist=False)
    
    print("test pairings with D-twist")
    #Fq6D.<wD> = Fq.extension(z**6 - 2)
    Fq6D = Fq.extension(z**6 - 2, names=('wD',),proof=False); (wD,) = Fq6D._first_ngens(1)
    E6D = EllipticCurve([Fq6D(0),Fq6D(-1)])
    
    test_G2_Frobenius_eigenvalue_MD_twist(E6D,E_D,r,c2,D_twist=True)
    test_bilinear_MillerLoop_BW6_761_MD_twist(E,E6D,E_D,r,c,c2,u0,D_twist=True)
    test_bilinear_MillerLoop_2NAF_BW6_761_MD_twist(E,E6D,E_D,r,c,c2,u0,D_twist=True)
    test_MillerLoop_ate_all_BW6_761_MD_twist(E,E6D,E_D,r,c,c2,u0,D_twist=True)
    
    test_FinalExp_BW6_761(Fq6D,r,u0)
    test_FinalExp_BW6_761(Fq6D,r,u0,expected_exponent=e0*ee)
    test_OptimalAtePairing_BW6_761_MD_twist(E,E6D,E_D,r,c,c2,u0,D_twist=True)
    
    test_double_line_H_a0_twist6_AKLGL(E,E_D,Fq6D,r,c,c2,D_twist=True,verbose=False)
    test_add_line_H_a0_twist6_AKLGL(E,E_D,Fq6D,r,c,c2,D_twist=True,verbose=False)
    test_sparse_mult_D6_twist(Fq6D)
    
    test_MillerFunction_ate_AKLGL(E,E_D,Fq6D,r,c,c2,u0,D_twist=True)
    
    test_MillerLoop_ate_AKLGL_naive_MD_twist(E,E_D,Fq6D,r,c,c2,u0,D_twist=True)
    test_MillerLoop_ate_AKLGL_MD_twist(E,E_D,Fq6D,r,c,c2,u0,D_twist=True)
    test_MillerLoop_ate_2NAF_AKLGL_MD_twist(E,E_D,Fq6D,r,c,c2,u0,D_twist=True)
    test_MillerLoop_ate_all_AKLGL_MD_twist(E,E_D,Fq6D,r,c,c2,u0,D_twist=True)
    test_MillerLoop_all_BW6_761_and_AKLGL_MD_twist(E,E6D,E_D,r,c,c2,u0,D_twist=True)
    
    
    script_fast_final_exponentiation()
    cost_pairing_BW6_761()
